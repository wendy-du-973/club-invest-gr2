
 
<script type="text/javascript">
    $(document).ready(function() {
     $('#table').DataTable( {
         "scrollCollapse": true,
        "paging": true,
        "scrollX": true
    } );
} );
</script>

<?php 
$data= $this->r->getController()->getUserRequest()->findAllUsers();
	if (count($data)>0) {
		?>
		<table id="table" class=" table table-striped table-responsive table-bordered">
			<thead>
				<tr>
 					<th scope="col">Nom</th>
					<th scope="col">Prenom</th>
					<th scope="col">Email</th>
					<th scope="col">Tel</th>
					<th scope="col">Status</th>
					<th scope="col">Role</th>
					<th scope="col">Bloquer / Debloquer</th>
					<th scope="col">Changer de Role</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data as $user) {
					?>
					<tr>
 						<th scope="col"><?php echo $user['nom']; ?></th>
						<th scope="col"><?php echo $user['prenom']; ?></th>
						<th scope="col"><?php echo $user['email']; ?></th>
						<th scope="col"><?php echo $user['tel']; ?></th>
						<th scope="col">
							<?php if (strcmp($user['status'],'lock')==0): ?>
								<span class="badge bg-warning"> Bloqué </span>
							<?php else: ?>
								<span class="badge bg-primary">operationnel</span>
							<?php endif ?>
										
						</th>
						<th scope="col">
							<?php 
							if($user['role']==="admin"){
								?>
								<a href="#" class="btn btn-danger"><?php echo $user['role']; ?></a>
								<?php  
							}else {
								?>
								<a href="#" class="btn btn-default"><?php echo $user['role']; ?></a>
								<?php  
							}
						 ?>
						</th>
						<th scope="col">
							<?php if (!strcmp($user['role'],'admin')==0): ?>
								
							    	<form method="POST" action="index.php?page=<?php echo $this->r->getLockUser(); ?>">

									<input type="hidden" name="id" value="<?php echo $user['id']; ?>">		
									<?php if (strcmp($user['status'],'lock')): ?>
									<input type="submit" name="" value="bloquer" class="btn btn-danger">

									<?php else: ?>
									<input type="submit" name="lockUser" value="debloquer" class="btn btn-primary">
								<?php  endif ?>
							</form>
							<?php endif ?>
							
							       					 
 					</th>
						<th scope="col">

							<form method="POST" action="index.php?page=<?php echo $this->r->setRoleUrl(); ?>">
									<input type="hidden" name="id" value="<?php echo $user['id']; ?>"><?php  
							if($user['role']=='simple'){
							
							}else if( $user['role']=='admin' ){

							}else{
								?>
									<select name="role" class="form-select">
										<option>com</option>
										<option>compta</option>
									</select>
									<input type="submit" name="" value="Changer De Role" class="btn btn-info">
 								<?php  
							}
							 ?>
						</form>
						</th>

					</tr>

					<?php  
				} ?>
			</tbody>
		</table>
		<?php  
	}else{
	    $this->v->alertDanger($this->v->setError("Aucune donnee trouvée "));
 	}
 ?>


 


 