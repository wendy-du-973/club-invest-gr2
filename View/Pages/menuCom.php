<nav class="navbar navbar-expand-lg navbar-dark bg-primary"   >
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Chez Nous</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <!-- <span class="navbar-toggler-icon"></span> -->
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="?page=acceuil">Acceuil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active " href="?page=presentation">Presentation</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="?page=contact">Contact</a>
        </li>

        <li class="nav-item">
          <a class="nav-link active" href="?page=logout">LogOut</a>
        </li>

        <li class="nav-item">
          <a class="nav-link active" href="?page=forum">forum</a>
        </li>

        <li class="nav-item">
          <a class="nav-link active" href="?page=newsLetter">NewsLetter</a>
        </li>

        <li class="nav-item">
          <a class="nav-link active" href="?page=vote">vote</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link active" href="?page=portefeuil">portefeuil</a>
        </li>
      </ul>
      <span class="navbar-text">
        Navbar text with an inline element
      </span>
    </div>
  </div>
</nav>