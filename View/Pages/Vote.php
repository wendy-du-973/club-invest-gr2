<div class="row">
	<div class="col-md-7">
		<?php echo $this->r->getController()->liseVotes(); ?>
	</div>
	<div class="col-md-5 ">

<?php // echo $data['email']; ?>

<?php 
	if(!$this->r->getController()->isVotingNow()){
		?>
		<form method="post" action="index.php?page=<?php echo $this->r->startVote(); ?>">
			<input type="hidden" name="idUser"  value="<?php echo  $this->r->getUser()['id']; ?>" >
		    <?php if (array_key_exists('sujet', $data)) {
		        ?>
		        <input type="text" name="sujet"  value="<?php echo $data['sujet']; ?>" class="form-control mb-3" >
		        <?php  
		    } else {?> 
		    <input type="text" name="sujet" placeholder="le sujet " class="form-control mb-3" >
		    <?php } ?>

		    <?php 
		    if (array_key_exists('duree', $data)) {
		        ?>

		        <input type="number" value="<?php echo $data['duree'];?>" name="duree" placeholder="duree"class="form-control" >
		            
		      <?php  
		     } else {?>
		        <input type="number"  class="form-control mb-3 mt-3" name="duree" placeholder="la duree">
		            
		    <?php } ?>
		    <input type="submit" name="" value="Debuter Le Vote  " class="btn btn-primary col-md-12">

		</form>	
		<?php  
	}else {
		if ($this->r->getController()->hasAllreadyVoted()>0) {
			$this->r->getController()->getView()->HaveAllreadyVoted();
		}else{
		?> 
	 <div class="card bg-light card-vote" >
  <div class="card-body">
    <h5 class="card-title"><?php echo $this->r->getController()->getCurrentVote()['sujet']; ?></h5>
    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
    		<?php if ($this->r->getController()->nbVotant()==$this->r->getController()->nbUserCanVote()): ?>
    			<p class="card-text text-light bg-success">tout le mode a voté </p>
	    		<?php else: ?>
    			<p class="card-text text-light bg-danger">tout le mode n'a pas encore voté un peu de patience</p>

    		<?php endif ?>
    		<div class="row"> 
 		    		<div class="row">
		    			<div class="col-md-6">
		    				<!-- /****** voter ********/ -->
		    				 <form method="post" action="index.php?page=<? echo $this->r->ouiVotes(); ?>">
								<input type="hidden" name="idUser"  value="<?php echo  $this->r->getUser()['id']; ?>" >
								<input type="hidden" name="idVote"  value="<?php echo  $this->r->getController()->getCurrentVote()['id']; ?>" >
								 <input type="radio" id="html" name="choix" value="oui">
										 <label for="oui">Oui</label>
					 			 <input type="radio" id="non" name="choix" value="non">
								 <label for="non">Non</label><br>
 						 		<?php if ($this->r->getController()->isUserLocked()==0): ?>
									<!-- $this->r->getController()->getView()->canNotUserVote(); -->
							 		<span class="badge badge-danger text-danger">Vous ne pouvez pas voter</span>

								<?php else: ?>
									<input type="submit" name="voter" value="Voter" class="btn btn-primary card-link"> 

								<?php endif ?>

							</form>
		    			</div>
		    			<div class="col-md-6">
		    				<!-- /****** terminee voter ********/ -->
		    				
						 		<?php if ($this->r->getController()->isUserLocked()!==0): ?>
 									<!-- $this->r->getController()->getView()->canNotUserVote(); -->
									 <?php if ($this->r->getController()->isItYourVote()): ?>
									 	<form method="post" action="index.php?page=<? echo $this->r->endVotes(); ?>">
										<input type="hidden" name="idUser"  value="<?php echo  $this->r->getUser()['id']; ?>" >
										<input type="hidden" name="idVote"  value="<?php echo  $this->r->getController()->getCurrentVote()['id']; ?>" >
 	 									<input type="submit" name="Endvote" value="Mettre Fin Au Vote" class="btn 	btn-primary card-link"> 
		 								</form>
								     	<?php else: ?>
 
								     <?php endif ?>
								 

								<?php endif ?>

 					 		

		    			</div>
		    		</div>

 			     
 	 	</div>
  </div>
</div>
			   
 	 
		
			    </div>

			   </div>
			</div>
    <?php  
	}
	}
 ?>

	</div>
</div>