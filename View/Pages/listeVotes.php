 
 
<script type="text/javascript">
    $(document).ready(function() {
     $('#table').DataTable( {
         "scrollCollapse": true,
        "paging": true,
        "scrollX": true
    } );
} );
</script>


<?php 
	if (count($data)>0) {
		?>
		<div class='jumbotron col-auto shadow p-3 mb-5  rounded'>
		<table id="table" class="table table-striped table-responsive table-bordered align-middle">
			<thead>
				<tr>
					<th scope="col">Sujet</th>
					<th scope="col">Durée</th>
					<th scope="col">Oui</th>
					<th scope="col">Non</th>
					<th scope="col">Status</th>
					<th scope="col">Résultat</th>
 				</tr>
			</thead>
			<tbody>


				<?php foreach ($data as $vote) {
					if ($vote['status']=='encours') {
						?>

					<tr class="table-warning">
						<td scope="col"><?php echo $vote['sujet']; ?></td>
						<td scope="col"><?php echo $vote['duree']; ?></td>
						<td scope="col"><?php echo $vote['oui']; ?></td>
						<td scope="col"><?php echo $vote['non']; ?></td>
						<td scope="col"><?php echo $vote['status']; ?></td>
						<td scope="col"><?php echo "Resultat"; ?> </th>
 					</tr>	
						<?php  
					}
					?>
					<tr>
						<td scope="col"><?php echo $vote['sujet']; ?></td>
						<td scope="col"><?php echo $vote['duree']; ?></td>
						<td scope="col"><?php echo $vote['oui']; ?></td>
						<td scope="col"><?php echo $vote['non']; ?></td>
						<td scope="col">
                         <?php echo $vote['status']; ?></td>
						 <?php if($vote['oui']>$vote['non']){
							echo "<th style='color:#20c997' scope='col'>Oui</th>";
						}elseif($vote['oui']<$vote['non']){
							echo "<th style='color:#dc3545' scope='col'>Non</th>";
						}else{
							echo "<th style='color:#fd7e14' scope='col'>Neutre</th>";
						}?>
 					</tr>
 					<?php  
					}
				?>
			</tbody>

		</table>
		</div>

<?php } ?>