<div class='jumbotron shadow p-3 mb-5 bg-body rounded'>
    <h5 class='display-4'>Rédaction des Newsletters 📝</h5>
    <p>Espace réservé uniquement aux chargés de communication.</p>

</div>
<div class="container row ">
    <div class=" jumbotron shadow col-12 p-3 mb-5  rounded">
	<h5>Votre newsletter sera envoyée à tous les abonnés.</h5>
	<hr>
        <?php // echo $data['email']; ?>
        <form method="post" action="index.php?page=<?php echo $this->r->sendNewsletterTrai(); ?>">
            <?php if (array_key_exists('titre', $data)) {
				?>
            <input  type="text" name="titre" value="<?php echo $data['titre']; ?>" class="form-control mb-3" required>
            <?php   
			} else {?>
            <input type="text" name="titre" placeholder="Le titre " class="form-control mb-3" required>
            <?php } ?>
            <input type="hidden" name="idUser" value="<?php echo  $this->r->getUser()['id']; ?>" required>
            <?php 
			if (array_key_exists('contenu', $data)) {
				 ?>

            <textarea name="contenu" class="form-control" rows="10" required>
					 <?php echo $data['contenu'];?>
				 </textarea> <?php  
			 } else {?>
            <textarea class="form-control mb-3 mt-3" rows="10" name="contenu" placeholder="Le Contenu" required>

				 </textarea>
            <?php } ?>
            <input type="submit" name="" value="Envoyer" class="btn btn-primary ">

        </form>
    </div>
    <div class="jumbotron col-12  shadow p-3 mb-5  rounded">
		<h5>Historique.</h5>
		<hr>
          <table class=" mb-3 mt-3 table table-bordered table-responsive table-striped">
            <thead>
                <tr>
                    <th scope="col">titre</th>
                    <th scope="col">date</th>
                </tr>
            </thead>    
            <tbody>
                <?php foreach($this->r->getController()->getAllNewsletter() as $news ): ?>
                        <tr>
                        <?php if (strcmp($news['status'],"public")!==0): ?>
                            <td>
                                <?php echo $news['titre']; ?>
                            </td>
                            <td>
                                <?php echo $news['date']; ?>
                            </td>
                        <?php endif ?>
                    </tr>
                <?php endforeach ?>
                <?php  ?>
            </tbody>
        </table>

    </div>

</div>