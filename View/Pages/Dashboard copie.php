<?php 

// set_include_path("../src");

/* Inclusion des classes utilisées dans ce fichier */
// require_once("../../Router.php");
// $Router = new Router();
 // $Router->main();


  ?><!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title> Admin </title>
  </head>
  <body>
<nav class="navbar navbar-dark bg-primary fixed-top flex-md-nowrap p-0 shadow">
  <!-- Navbar content -->
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">dashboard</a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <input type="text" name="" class="form-control" placeholder="">
  <ul class="navbar-nav px-3">
  	<li class="nav-item text-nowrap">
  		<a class="nav-link">logout</a>
  	</li>
  </ul>
</nav>

<div class="container-fluid">
	<div class="row">
		 
		<div class="col-md-2 bg-light d-none d-md-block sidebar">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			 <?php // $this->r->getView()->showMenuAdmin(); ?>
		</div>
	</div>
</div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>

<style type="text/css">
	.sidebar{
		position: fixed;
		top: 0;
		bottom: 0;
		left: 0;
		border-right: 1px solid black;
	}
	.sidebar-light{
		position: sticky;
		top: 0 ;
		height:calc(100vh - 70px);
	}
</style>