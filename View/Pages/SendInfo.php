
<div class="row mb-3 mt-3">
	<div class="col-md-7">
		

		<?php // echo $data['email']; ?>
		<form method="post" action="index.php?page=<?php echo $this->r->sendInfoToEveryone(); ?>">
			<?php if (array_key_exists('titre', $data)) {
				?>
				<input type="text" name="titre"  value="<?php echo $data['titre']; ?>" class="form-control mb-3" >
				<?php  
			} else {?>
			<input type="text" name="titre" placeholder="le titre " class="form-control mb-3" >
			<?php } ?>
			<input type="hidden" name="idUser"  value="<?php echo  $this->r->getUser()['id']; ?>" >
			<?php 
			if (array_key_exists('contenu', $data)) {
			 	?>

			 	<textarea name="contenu" class="form-control" rows="10">
			 		<?php echo $data['contenu'];?>
			 	</textarea>
			 		 	<?php  

			 } else {?>
			 	<textarea class="form-control mb-3 mt-3" rows="10" name="contenu" placeholder="Le Contenu">
			 		
			 	</textarea>
			<?php } ?>
		  	<input type="submit" name="" value="Envoyer l'information  " class="btn btn-primary col-md-12">

		</form>	
	</div>
	<div class="col-md-5">
		<h4>liste des info deja envoyees</h4>

		<table class=" mb-3 mt-3 table table-striped">
			<thead>
				<tr>
					<th scope="col">titre</th>
					<th scope="col">date</th>
 				</tr>
			</thead>	
			<tbody>
				<?php foreach($this->r->getController()->getAllNewsletter() as $news ): ?>
						<tr>
						<?php if (strcmp($news['status'],"private")==0): ?>
							<td>
								<?php echo $news['titre']; ?>
							</td>
							<td>
								<?php echo $news['date']; ?>
							</td>
						<?php endif ?>
					</tr>
				<?php endforeach ?>
				<?php  ?>
			</tbody>
		</table>
 	</div>

</div>