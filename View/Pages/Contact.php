
 
<div class='   container p-3 mb-5 bg-body rounded'>
    <h5 class='display-4'>Nous contacter 🔔</h5>

</div>
<div class="row">
    <div class="   col-md-7  container p-3 mb-5 bg-body rounded  ">
                
        <?php // echo $data['email']; ?>
        <form method="post" action="index.php?page=<?php echo $this->r->contactUsUrl(); ?>">
            <?php if (array_key_exists('email', $data)) {
                ?>
                <input type="text" name="email"  value="<?php echo $data['email']; ?>" class="form-control mb-3" >
                <?php  
            } else {?>
            <input type="text" name="email" placeholder="Adresse mail" class="form-control mb-3" >
            <?php } ?>

            <?php 
            if (array_key_exists('contenu', $data)) {
                ?>

                <textarea name="contenu" class="form-control" rows="6" placeholder="Votre avis nous intéresse">
                    <?php echo $data['contenu'];?>
                </textarea>     <?php  
             } else {?>
                <textarea class="form-control mb-3 mt-3" rows="6" name="contenu" placeholder="Votre avis nous intéresse"></textarea>
            <?php } ?>
            <input type="submit" name="" value="S'enregistrer" class="btn btn-primary col-md-12">

        </form>
    </div>
    <div class="col-md-5">
        
<div class='jumbotron shadow p-3 mb-5 bg-body rounded'>
    <div class='container'>
      <h3>Contactez-nous</h3>
      <h6>Vous avez la possibilité de contacter les membres.</h6>
        <ul class="contact">
            <li><a class=" text-info badge badge-info" href="mailto:?to=slim.souissi@unicaen.fr,slim.souissi@unicaen.fr">Slim SOUISSI ✉</a></li>
            <li><a class="text-info badge badge-info" href="mailto:?to=21606478@etu.unicaen.fr,21606478@etu.unicaen.fr">Vincent QUELLIER ✉</a></li>
            
        </ul>
        
    </div>
  </div>


    </div>
</div>