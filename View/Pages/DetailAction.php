<?php if ($data): ?>

	<div class="container mb-3 ">
		<form method="POST" class="mt-3 btn-dark text-light mb-3" action="index.php?page=<?php echo $this->r->editOrDeleteAction(); ?>">

			<input type="hidden" name="id" value="<?php echo $data['id']; ?>">
			<input type="text" name="nom" value="<?php echo $data['nom']; ?>" class="form-control mb-3">
			<input type="text" name="prixActu" value="<?php echo $data['prixActu']; ?>" class="form-control mb-3">
			<input type="text" name="prixAchat" value="<?php echo $data['prixAchat']; ?>" class="form-control mb-3">
			<div class="row mt-3">
				<div class="col-md-6">
					<input type="submit" name="edit" value="Editer l'Action" class="btn btn-success">
				</div>
				<div class="col-md-6">
					<input type="submit" name="delete" value="supprimer l'Action" class="btn btn-danger">
				</div>
			</div>
		</form>
	</div>

<?php endif ?>