<div class='jumbotron shadow p-3 mb-5 bg-body rounded'>
    <h5 class='display-4'>Espace forum 💬</h5>
    <p>Retrouvez ici l’espace de discussions publique. Les discussions y sont archivées ce qui permet une communication asynchrone.</p>
	<p>🔶: En cours  -  ✅ : Terminé </p>
</div>
<div class="row align-items-start">
	<div class="col-md-8 " >
		
		 <?php 
			$data= $this->r->getController()->getRequestSubject()->findAllSujet();


		  ?>
		  <!-- /******************************/ -->
		  	<div id="accordion">
		  	<?php foreach ($data as $sujet): ?>

			  <div class="jumbotron  shadow rounded" >
			  	<?php if (strcmp($sujet['status'],'encours')==0): ?>
			  		
			  		 <div class="card-header bg-warning text-white rounded" id="headingOne" >
			      <h5 class="mb-0">
			        <button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#<?php echo $sujet['id']; ?>" aria-expanded="false" aria-controls="collapseOne">
			          <?php echo "🔶 ".$sujet['createdAt']." - ".$sujet['sujet']; 
					  		
					  ?> 

			        </button>

 			      </h5>
			    </div>
				<?php else: ?>
				 <div class="card-header bg-success text-white" id="headingOne">
			      <h5 class="mb-0">
			        <button class="btn btn-link " data-toggle="collapse" data-target="#<?php echo $sujet['id']; ?>" aria-expanded="false" aria-controls="collapseOne">
			          <?php echo "✅ ".$sujet['createdAt']." - ".$sujet['sujet'] ; ?>
			        </button>
 			      </h5>
			    </div>
			  	<?php endif ?>
 

			   

			    <div id="<?php echo $sujet['id']; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
			      <div class="card-body">
					<?php if (strcmp($sujet['status'],'encours')==0){ ?>
			      	<?php $this->r->getController()->getAllCommentForThisSujet($sujet['id']); ?>
			      	<form method="POST" action="index.php?page=<?php echo $this->r->commentThisSujet(); ?>">
					  <textarea  name="commentaire" class="form-control" rows="4" placeholder="Commentaire"></textarea>
			      		<input type="hidden" name="idUser" value="<?php echo  $this->r->getUser()['id']; ?>">
			      		<input type="hidden" name="idSujet" value="<?php echo $sujet['id']; ?>">
						  <br>
			      		<input type="submit" name="" class="btn btn-secondary btn-sm" value="Envoyer">
			      	</form>
					  <?php }else
					  		echo "<h6>Discussion terminée</h6>"?>
			      </div>
			    </div>
			    <div class="card-footer">
			    	<?php if ($this->r->getUser()['id']===$sujet['idUser']): ?>
			      	<form method="POST" action="index.php?page=<?php echo $this->r->termineeSujetUrl(); ?>">
	          			<input type="hidden" name="idUser"  value="<?php echo  $this->r->getUser()['id']; ?>" >
	          			<input type="hidden" name="idSujet"	value="<?php echo $sujet['id']; ?>" >
	          			<input type="submit="  name="" value="x" class="btn btn-danger btn-sm">
	          		</form>
 			  			<?php endif ?>
			    </div>
			  </div>
	  		<?php endforeach ?> 
   
    </div>
		  <!-- /******************************/ -->

	</div>
	<div class="jumbotron col-md-4 shadow p-3 mb-5 bg-body rounded ">
	<h5>Lancer un nouveau sujet 📮</h5>
 
		<!-- <form method="POST"  id="sendTchat" action="View/Pages/SendTchat.php"> -->
			<form id="sendTchat" method="POST" action="index.php?page=<?php echo $this->r->openSubjetUrl(); ?>">
			<textarea id="msg" name="sujet" class="form-control" rows="4" placeholder="Sujet"></textarea>
			<input type="hidden" name="idUser"  value="<?php echo  $this->r->getUser()['id']; ?>" >
			<span id="response"></span>
			<br>
		 <input type="submit" name="openSubject"  class="form-control btn btn-primary btn-sm" value="Envoyer">
	</form>
	</div>
</div>

	<?php // require_once('ListeMessage.php'); ?>




 <script type="text/javascript" src="./upload/js/Script.js" defer=""></script>

<!-- __________________IMPORTED_________________________ -->
     <!-- DIRECT CHAT -->
                <div class="card direct-chat direct-chat-warning">
                  <div class="card-header">
                    <h3 class="card-title">Direct Chat</h3>

                    <div class="card-tools">
                      <span title="3 New Messages" class="badge badge-warning">3</span>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" title="Contacts" data-widget="chat-pane-toggle">
                        <i class="fas fa-comments"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                      <!-- Message. Default to the left -->
                      <div class="direct-chat-msg">
                        <div class="direct-chat-infos clearfix">
                          <span class="direct-chat-name float-left">Alexander Pierce</span>
                          <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                          Is this template really for free? That's unbelievable!
                        </div>
                        <!-- /.direct-chat-text -->
                      </div>
                      <!-- /.direct-chat-msg -->

                      <!-- Message to the right -->
                      <div class="direct-chat-msg right">
                        <div class="direct-chat-infos clearfix">
                          <span class="direct-chat-name float-right">Sarah Bullock</span>
                          <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                          You better believe it!
                        </div>
                        <!-- /.direct-chat-text -->
                      </div>
                      <!-- /.direct-chat-msg -->

                      <!-- Message. Default to the left -->
                      <div class="direct-chat-msg">
                        <div class="direct-chat-infos clearfix">
                          <span class="direct-chat-name float-left">Alexander Pierce</span>
                          <span class="direct-chat-timestamp float-right">23 Jan 5:37 pm</span>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                          Working with AdminLTE on a great new app! Wanna join?
                        </div>
                        <!-- /.direct-chat-text -->
                      </div>
                      <!-- /.direct-chat-msg -->

                      <!-- Message to the right -->
                      <div class="direct-chat-msg right">
                        <div class="direct-chat-infos clearfix">
                          <span class="direct-chat-name float-right">Sarah Bullock</span>
                          <span class="direct-chat-timestamp float-left">23 Jan 6:10 pm</span>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                          I would love to.
                        </div>
                        <!-- /.direct-chat-text -->
                      </div>
                      <!-- /.direct-chat-msg -->

                    </div>
                    <!--/.direct-chat-messages-->

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts">
                      <ul class="contacts-list">
                        <li>
                          <a href="#">
                            <img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Avatar">

                            <div class="contacts-list-info">
                              <span class="contacts-list-name">
                                Count Dracula
                                <small class="contacts-list-date float-right">2/28/2015</small>
                              </span>
                              <span class="contacts-list-msg">How have you been? I was...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                          </a>
                        </li>
                        <!-- End Contact Item -->
                        <li>
                          <a href="#">
                            <img class="contacts-list-img" src="dist/img/user7-128x128.jpg" alt="User Avatar">

                            <div class="contacts-list-info">
                              <span class="contacts-list-name">
                                Sarah Doe
                                <small class="contacts-list-date float-right">2/23/2015</small>
                              </span>
                              <span class="contacts-list-msg">I will be waiting for...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                          </a>
                        </li>
                        <!-- End Contact Item -->
                        <li>
                          <a href="#">
                            <img class="contacts-list-img" src="dist/img/user3-128x128.jpg" alt="User Avatar">

                            <div class="contacts-list-info">
                              <span class="contacts-list-name">
                                Nadia Jolie
                                <small class="contacts-list-date float-right">2/20/2015</small>
                              </span>
                              <span class="contacts-list-msg">I'll call you back at...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                          </a>
                        </li>
                        <!-- End Contact Item -->
                        <li>
                          <a href="#">
                            <img class="contacts-list-img" src="dist/img/user5-128x128.jpg" alt="User Avatar">

                            <div class="contacts-list-info">
                              <span class="contacts-list-name">
                                Nora S. Vans
                                <small class="contacts-list-date float-right">2/10/2015</small>
                              </span>
                              <span class="contacts-list-msg">Where is your new...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                          </a>
                        </li>
                        <!-- End Contact Item -->
                        <li>
                          <a href="#">
                            <img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Avatar">

                            <div class="contacts-list-info">
                              <span class="contacts-list-name">
                                John K.
                                <small class="contacts-list-date float-right">1/27/2015</small>
                              </span>
                              <span class="contacts-list-msg">Can I take a look at...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                          </a>
                        </li>
                        <!-- End Contact Item -->
                        <li>
                          <a href="#">
                            <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Avatar">

                            <div class="contacts-list-info">
                              <span class="contacts-list-name">
                                Kenneth M.
                                <small class="contacts-list-date float-right">1/4/2015</small>
                              </span>
                              <span class="contacts-list-msg">Never mind I found...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                          </a>
                        </li>
                        <!-- End Contact Item -->
                      </ul>
                      <!-- /.contacts-list -->
                    </div>
                    <!-- /.direct-chat-pane -->
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <form action="#" method="post">
                      <div class="input-group">
                        <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                        <span class="input-group-append">
                          <button type="button" class="btn btn-warning">Send</button>
                        </span>
                      </div>
                    </form>
                  </div>
                  <!-- /.card-footer-->
                </div>