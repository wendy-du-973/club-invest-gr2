 
<!doctype html>
<html lang='fr'>
  <head>
    <!-- Required meta tags -->
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>

    <!-- Bootstrap CSS -->
    
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="View/Pages/assets/Style.css" rel="stylesheet">  
<title>Accueil</title>
<!-- Custom styles for this template -->
<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

</head>

<body>



<main>

  <?php
  $user = isset($_SESSION['user'])? $_SESSION['user']:'';
  if($user===''){
    echo $_SESSION["user['role']"];
    echo "<div id='myCarousel' class='carousel slide' data-bs-ride='carousel'>
    <div class='carousel-indicators'>
      <button type='button' data-bs-target='#myCarousel' data-bs-slide-to='0' class='active' aria-current='true' aria-label='Slide 1'></button>
      <button type='button' data-bs-target='#myCarousel' data-bs-slide-to='1' aria-label='Slide 2'></button>
      <button type='button' data-bs-target='#myCarousel' data-bs-slide-to='2' aria-label='Slide 3'></button>
    </div>
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img class='bd-placeholder-img' src='View/image/iae.jpg' width='100%' height='60%' />
        <div class='container'>
          <div class='carousel-caption text-start'>
            <h1>IUP BFA CAEN.</h1>
            <p>Club d'investissement de Caen Normandie depuis 2021.</p>
            
          </div>
        </div>
      </div>
      <div class='carousel-item'>
      <img class='bd-placeholder-img' src='View/image/iae2.jpg' width='100%' height='60%' />
        <div class='container'>
          <div class='carousel-caption'>
            <h1>Another example headline.</h1>
            <p>Some representative placeholder content for the second slide of the carousel.</p>
            
          </div>
        </div>
      </div>
      <div class='carousel-item'>
      <img class='bd-placeholder-img' src='View/image/iae3.jpg' width='100%' height='60%' />
        <div class='container'>
          <div class='carousel-caption text-end'>
            <h1>One more for good measure.</h1>
            <p>Some representative placeholder content for the third slide of this carousel.</p>
        </div>
        </div>
      </div>
    </div>
    <button class='carousel-control-prev' type='button' data-bs-target='#myCarousel' data-bs-slide='prev'>
      <span class='carousel-control-prev-icon' aria-hidden='true'></span>
      <span class='visually-hidden'>Previous</span>
    </button>
    <button class='carousel-control-next' type='button' data-bs-target='#myCarousel' data-bs-slide='next'>
      <span class='carousel-control-next-icon' aria-hidden='true'></span>
      <span class='visually-hidden'>Next</span>
    </button>
  </div>


  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->
  <br>
  <div class='container marketing'>

    <!-- Three columns of text below the carousel -->
    <div class='row'>
      <div class='col-lg-4 text-center'>
       <img class='bd-placeholder-img' src='View/image/recherche.png' width='80' height='80' />
        <h2>Découvrir</h2>
        <p>Vous êtes à l’université de Caen et vous souhaitez rejoindre un club d’investissement. On est là pour vous, que vous soyez débutants ou experts, rejoignez le club d’investissement de l'IAE basé à Caen. Pour cela contactez l'un des membres du club afin d’avoir les modalités de recrutement.
        </p>
        <p><a class='btn btn-secondary' href='#'>Contacter &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class='col-lg-4 text-center'>
        <img class='bd-placeholder-img' src='View/image/email.png' width='80' height='80' />
        <h2>Newsletter</h2>
        <p>Vous hésitez à nous rejoindre ? Alors vous avez la possibilité de vous abonner à notre newsletter afin de recevoir des news qui peuvent vous intéresser.</p>
        <p><a class='btn btn-secondary' href='?page=contact'>S'abonner &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class='col-lg-4 text-center'>
        <img class='bd-placeholder-img' src='View/image/membres.png' width='80' height='80' />
        <h2>Espace membre</h2>
        <p>Vous êtes déjà membre ou vous venez tout juste de recevoir un mail de confirmation de votre inscription. Alors vous pouvez dès à présent vous connecter à votre espace membre.</p>
        <p><a class='btn btn-secondary' href='?page=login'>Se connecter &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
    <hr class='featurette-divider'>

    <div class='row featurette'>
      <div class='col-md-7'>
        <h2 class='featurette-heading'>First featurette heading. <span class='text-muted'>It’ll blow your mind.</span></h2>
        <p class='lead'>Some great placeholder content for the first featurette here. Imagine some exciting prose here.</p>
      </div>
      <div class='col-md-5'>
        <svg class='bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto' width='500' height='500' xmlns='http://www.w3.org/2000/svg' role='img' aria-label='Placeholder: 500x500' preserveAspectRatio='xMidYMid slice' focusable='false'><title>Placeholder</title><rect width='100%' height='100%' fill='#eee'/><text x='50%' y='50%' fill='#aaa' dy='.3em'>500x500</text></svg>

      </div>
    </div>

    <hr class='featurette-divider'>

    <div class='row featurette'>
      <div class='col-md-7 order-md-2'>
        <h2 class='featurette-heading'>Oh yeah, it’s that good. <span class='text-muted'>See for yourself.</span></h2>
        <p class='lead'>Another featurette? Of course. More placeholder content here to give you an idea of how this layout would work with some actual real-world content in place.</p>
      </div>
      <div class='col-md-5 order-md-1'>
        <svg class='bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto' width='500' height='500' xmlns='http://www.w3.org/2000/svg' role='img' aria-label='Placeholder: 500x500' preserveAspectRatio='xMidYMid slice' focusable='false'><title>Placeholder</title><rect width='100%' height='100%' fill='#eee'/><text x='50%' y='50%' fill='#aaa' dy='.3em'>500x500</text></svg>

      </div>
    </div>

    <hr class='featurette-divider'>

    <div class='row featurette'>
      <div class='col-md-7'>
        <h2 class='featurette-heading'>And lastly, this one. <span class='text-muted'>Checkmate.</span></h2>
        <p class='lead'>And yes, this is the last block of representative placeholder content. Again, not really intended to be actually read, simply here to give you a better view of what this would look like with some actual content. Your content.</p>
      </div>
      <div class='col-md-5'>
        <svg class='bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto' width='500' height='500' xmlns='http://www.w3.org/2000/svg' role='img' aria-label='Placeholder: 500x500' preserveAspectRatio='xMidYMid slice' focusable='false'><title>Placeholder</title><rect width='100%' height='100%' fill='#eee'/><text x='50%' y='50%' fill='#aaa' dy='.3em'>500x500</text></svg>

      </div>
    </div>

    <hr class='featurette-divider'>

    <!-- /END THE FEATURETTES -->

  </div>";


  }else{
    echo "<div id='myCarousel' class='carousel slide' data-bs-ride='carousel'>
    <div class='carousel-indicators'>
      <button type='button' data-bs-target='#myCarousel' data-bs-slide-to='0' class='active' aria-current='true' aria-label='Slide 1'></button>
      <button type='button' data-bs-target='#myCarousel' data-bs-slide-to='1' aria-label='Slide 2'></button>
      <button type='button' data-bs-target='#myCarousel' data-bs-slide-to='2' aria-label='Slide 3'></button>
    </div>
    <div class='carousel-inner'>
      <div class='carousel-item active'>
        <img class='bd-placeholder-img' src='View/image/iae.jpg' width='100%' height='60%' />
        <div class='container'>
          <div class='carousel-caption text-start'>
            <h1>IUP BFA CAEN.</h1>
            <p>Club d'investissement de Caen Normandie depuis 2021.</p>
            
          </div>
        </div>
      </div>
      <div class='carousel-item'>
      <img class='bd-placeholder-img' src='View/image/iae2.jpg' width='100%' height='60%' />
        <div class='container'>
          <div class='carousel-caption'>
            <h1>Another example headline.</h1>
            <p>Some representative placeholder content for the second slide of the carousel.</p>
            <p><a class='btn btn-lg btn-primary' href='#'>Learn more</a></p>
          </div>
        </div>
      </div>
      <div class='carousel-item'>
      <img class='bd-placeholder-img' src='View/image/iae3.jpg' width='100%' height='60%' />
        <div class='container'>
          <div class='carousel-caption text-end'>
            <h1>One more for good measure.</h1>
            <p>Some representative placeholder content for the third slide of this carousel.</p>
            <p><a class='btn btn-lg btn-primary' href='#'>Browse gallery</a></p>
          </div>
        </div>
      </div>
    </div>
    <button class='carousel-control-prev' type='button' data-bs-target='#myCarousel' data-bs-slide='prev'>
      <span class='carousel-control-prev-icon' aria-hidden='true'></span>
      <span class='visually-hidden'>Previous</span>
    </button>
    <button class='carousel-control-next' type='button' data-bs-target='#myCarousel' data-bs-slide='next'>
      <span class='carousel-control-next-icon' aria-hidden='true'></span>
      <span class='visually-hidden'>Next</span>
    </button>
  </div>
  <div class='container'>
      <h1 class=display-3'>Bienvenu ".ucwords($_SESSION['user']['prenom'])." ".strtoupper($_SESSION['user']['nom'])."</h1>
      <p>Infos du jour ...[ ]</p>
    </div>


  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->
  <br>
  <div class='container marketing'>

    <!-- Three columns of text below the carousel -->
    <div class='row'>
      <div class='col-lg-4 text-center'>
       <img class='bd-placeholder-img' src='View/image/portefeuille.png' width='80' height='80' />
        <h2>Espace Personnel</h2>
        <p>Accède à ton portefeuille personnel. </p>
        <p><a class='btn btn-secondary' href='#'>Acceder &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class='col-lg-4 text-center'>
        <img class='bd-placeholder-img' src='View/image/membres.png' width='80' height='80' />
        <h2>Espace commune</h2>
        <p>Accede au portefeuille commun du club.</p>
        <p><a class='btn btn-secondary' href='#'>Acceder &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class='col-lg-4 text-center'>
        <img class='bd-placeholder-img' src='View/image/bourse1.png' width='80' height='80' />
        <h2>Cours boursier</h2>
        <p>Jete un coup d'oeil sur le marché financier</p>
        <p><a class='btn btn-secondary' href='?page=portefeuil'>Acceder &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
    <hr class='featurette-divider'>

    <div class='row featurette'>
      <div class='col-md-7'>
        <h2 class='featurette-heading'>First featurette heading. <span class='text-muted'>It’ll blow your mind.</span></h2>
        <p class='lead'>Some great placeholder content for the first featurette here. Imagine some exciting prose here.</p>
      </div>
      <div class='col-md-5'>
        <svg class='bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto' width='500' height='500' xmlns='http://www.w3.org/2000/svg' role='img' aria-label='Placeholder: 500x500' preserveAspectRatio='xMidYMid slice' focusable='false'><title>Placeholder</title><rect width='100%' height='100%' fill='#eee'/><text x='50%' y='50%' fill='#aaa' dy='.3em'>500x500</text></svg>

      </div>
    </div>

    <hr class='featurette-divider'>

    <div class='row featurette'>
      <div class='col-md-7 order-md-2'>
        <h2 class='featurette-heading'>Oh yeah, it’s that good. <span class='text-muted'>See for yourself.</span></h2>
        <p class='lead'>Another featurette? Of course. More placeholder content here to give you an idea of how this layout would work with some actual real-world content in place.</p>
      </div>
      <div class='col-md-5 order-md-1'>
        <svg class='bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto' width='500' height='500' xmlns='http://www.w3.org/2000/svg' role='img' aria-label='Placeholder: 500x500' preserveAspectRatio='xMidYMid slice' focusable='false'><title>Placeholder</title><rect width='100%' height='100%' fill='#eee'/><text x='50%' y='50%' fill='#aaa' dy='.3em'>500x500</text></svg>

      </div>
    </div>

    <hr class='featurette-divider'>

    <div class='row featurette'>
      <div class='col-md-7'>
        <h2 class='featurette-heading'>And lastly, this one. <span class='text-muted'>Checkmate.</span></h2>
        <p class='lead'>And yes, this is the last block of representative placeholder content. Again, not really intended to be actually read, simply here to give you a better view of what this would look like with some actual content. Your content.</p>
      </div>
      <div class='col-md-5'>
        <svg class='bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto' width='500' height='500' xmlns='http://www.w3.org/2000/svg' role='img' aria-label='Placeholder: 500x500' preserveAspectRatio='xMidYMid slice' focusable='false'><title>Placeholder</title><rect width='100%' height='100%' fill='#eee'/><text x='50%' y='50%' fill='#aaa' dy='.3em'>500x500</text></svg>

      </div>
    </div>

    <hr class='featurette-divider'>

    <!-- /END THE FEATURETTES -->

  </div>";
  }


?>

  

</main>

<footer class='container'>
  <p>&copy; Company 2021-2022</p>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>





