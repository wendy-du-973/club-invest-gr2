
<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4">
  <div class="form-bg">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="form-container">
                    <div class="form-icon"><i class="fa fa-user"></i></div>
                    <h4 class="title">Reinitialiser Votre Mot De Pass</h4>
                    <?php // echo $data['email']; ?>
<form method="post" action="index.php?page=<?php echo $this->r->reinitialize(); ?>">
	<?php if (array_key_exists('code', $data)) {
		?>
 		<input type="text" name="code"  value="<?php echo $data['code']; ?>" class="form-control mb-3" >
		<?php  
	} else {?>
	<input type="text" name="code" placeholder="votre code recu par mail" class="form-control mb-3" >
	<?php } ?>
	<input type="hidden" name="email"  value="<?php echo $data['email'] ?>" >
	<?php 
	if (array_key_exists('password', $data)) {
	 	?>
	<input type="password" name="password"  value="<?php echo $data['password'];?>" class="form-control mb-3" >
	 	<?php  
	 } else {?>
	<input type="password" name="password" placeholder="nouveau mot de pass" class="form-control mb-3" >
	<?php } ?>
		<?php 
	if (array_key_exists('conf_password', $data)) {
	 	?>
	<input type="password" name="conf_password"  value="<?php echo $data['conf_password'];?>" class="form-control mb-3" >
	 	<?php  
	 } else {?>

	<input type="password" name="conf_password" placeholder="confirmer le  mot de pass" class="form-control mb-3" >
	<?php } ?>
	<input type="submit" name="" value="Reinitialiser le mot de pass " class="btn btn-primary col-md-12">

</form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<div class="col-md-4"></div>
</div>
