<link
    rel=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
    type="text/css"
  />
  <script src=
"https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script
    src=
"https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
    type="text/javascript"
  ></script>
  <script src=
"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
  <script src=
"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
 

<div class="row mb-3">
	
</div>
<div class="row">
	<div class="col-md-7">
		<div class="row">
			<div class="col-md-4">
				<div class="form-floating">
				  <select class="form-select" id="floatingSelect" aria-label="Floating label select example">
				    <option selected>1 heure </option>
				    <option value="1">1 jour</option>
				    <option value="2">1 semaine</option>
				    <option value="3">1 mois</option>
				  </select>
				  <label for="floatingSelect">pour quelle periode</label>
				</div>
			</div>
			<div class="col-md-4">
				<h2>700 €</h2>
			</div>
		</div>
		<div class="row">
			<table class="table table-striped">
			  <thead>
			    <tr>
 			      <th scope="col">Nom Action</th>
 			      <th scope="col">prix actuel </th>
 			    </tr>
			  </thead>
			  <tbody>
			  	<?php 
			  	for ($i=0; $i < 5; $i++) { 
			  	 	?>
			  	 	 <tr>
 				      <td>Air France</td>
 				      <td>200 €</td>
 				      <td><button class="btn-success">Acheter </button></td>
				    </tr>
			  	 	<?php  
			  	 } ?>
			  	 	<?php 
			  	for ($i=0; $i < 5; $i++) { 
			  	 	?>
			  	 	 <tr>
 				      <td>Amazone </td>
 				      <td>100 €</td>
 				      <td><button class="btn-success">Acheter </button></td>

				    </tr>
			  	 	<?php  
			  	 } ?>
			  	 <?php 
			  	for ($i=0; $i < 5; $i++) { 
			  	 	?>
			  	 	 <tr>
 				      <td>Bitcoin</td>
				      <td>100 € </td>
  				      <td><button class="btn-success">Acheter </button></td>

				    </tr>
			  	 	<?php  
			  	 } ?>
			   
			  
			  </tbody>
			</table>
			<nav aria-label="...">
		  <ul class="pagination">
		    <li class="page-item disabled">
		      <a class="page-link" href="#" tabindex="-1">Previous</a>
		    </li>
		    <li class="page-item"><a class="page-link" href="#">1</a></li>
		    <li class="page-item active">
		      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
		    </li>
		    <li class="page-item"><a class="page-link" href="#">3</a></li>
		    <li class="page-item">
		      <a class="page-link" href="#">Next</a>
		    </li>
		  </ul>
		</nav>
		</div>
	</div>
	<div class="col-md-5">
		<div class="row mb-3">
			  <div class="">
		      <h2>Cours De l'Action  </h2>
		      <div>
		        <canvas id="myChart"></canvas>
		      </div>
		    </div>
		</div>
		<div class="row mt-3 mb-3" style="background: lightcoral;">
			<h6>Acheter au prix Actuel du marché</h6>
			<form >
			  <div class="mb-3">
			   <label for="customRange1" class="form-label">pourcentage de votre action</label>
				<input type="range" class="form-range" id="customRange1">
			  </div>
			  
			  <button type="submit" class="col-md-12 btn btn-success">Acheter</button>
			</form>
		</div>

		<div class="row mt-3 mb-3"  style="background: lightgray;">
			<h6>Acheter à un prix plutard (effet de levier )</h6>
			<form>
				<div class="mb-3">
					<div class="form-floating">
					  <input type="number" name="" class="form-control" placeholder="" id="floatingTextarea">
					  <label for="floatingTextarea">Le prix au quel vous voulez Acheter plutard</label>
					</div>
				</div>
			  <div class="mb-3">
			   <label for="customRange1" class="form-label">pourcentage de votre action</label>
				<input type="range" class="form-range" id="customRange1">
			  </div>
			  
			  <button type="submit" class="col-md-12 btn btn-success">Acheter</button>
			</form>
		</div>

</div>

 <script>
    var ctx = document.getElementById("myChart").getContext("2d");
    var myChart = new Chart(ctx, {
      type: "line",
      data: {
        labels: [
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday",
          "Sunday",
        ],
        datasets: [
          {
            label: "work load",
            data: [2, 9, 3, 17, 6, 3, 7],
            backgroundColor: "rgba(153,205,1,0.6)",
          },
          {
            label: "free hours",
            data: [2, 2, 5, 5, 2, 1, 10],
            backgroundColor: "rgba(155,153,10,0.6)",
          },
        ],
      },
    });
  </script>