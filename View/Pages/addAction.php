  

 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css"> 

<?php $v= $this->r->getController()->getView(); ?>
	<form method="POST" action="index.php?page=<?php echo $this->r->getAddActionTrai(); ?>">
<div class="mt-3 row">
				<div class="col-md-3">
				<div class="form-group">
				    <!-- <label for="exampleInputEmail1">Nom de l'action</label> -->
				    <?php if (array_key_exists('nom', $data)): ?>
					    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nom de l'action" name="nom" value="<?php echo $data['nom']; ?>">
					<?php else: ?>
				    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nom de l'action" name="nom">
						<?php endif ?>
				    <small id="emailHelp" class="form-text text-muted">Action Name.</small>
				  </div>
			</div>
			<div class="col-md-3">
			  <div class="form-group">
			    <!-- <label for="exampleInputEmail1">Prix Actuel de l'action</label> -->
				    <?php if (array_key_exists('prixActu', $data)): ?>

			    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Prix Actuel de l'action" name="prixActu" value="<?php echo $data['prixActu']; ?>">
			<?php else: ?>
				    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Prix Actuel de l'action" name="prixActu">
				<?php endif; ?>

			    <small id="emailHelp" class="form-text text-muted">Actual Action price.</small>
			  </div>
			</div>
			<div class="col-md-3">
				  <div class="form-group">
				    <?php if (array_key_exists('prixAchat', $data)): ?>
				    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Prix au que vous achetez de l'action" name="prixAchat" value="<?php echo $data['prixAchat']; ?>">
				<?php else: ?>
				    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Prix au que vous achetez de l'action" name="prixAchat">
				<?php endif; ?>
				    <small id="emailHelp" class="form-text text-muted">Action price that you buy.</small>
				  </div>
			</div>
			<div class="col-md-3">
				  <input type="submit" name="" value="Ajouter " class="form-control btn-lg btn btn-primary">
			</div>
</div>

	</form>

<div class="container">
	
<?php 
$data= $this->r->getController()->getActionRequest()->getAllAction();
	if (count($data)>0) {
		?>
		<table id="table" class=" table table-striped table-responsive table-bordered table-dark">
			<thead>
				<tr>
 					<th scope="col">Nom</th>
					<th scope="col">Prix d'achat</th>
					<th scope="col">Gain</th>
					<th scope="col">Prix Actuel</th>
					<th scope="col">date Achat</th>
					<th scope="col">Detail</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data as $action) {
					?>
					<tr>
 						<th scope="col"><?php echo $action['nom']; ?></th>
 						<th scope="col"><?php echo $action['prixAchat']; ?></th>
 						<th scope="col"><?php echo $action['prixAchat']- $action['prixActu']; ?></th>
 						<th scope="col"><?php echo $action['prixActu']; ?></th>
 						<th scope="col"><?php echo $action['date']; ?></th>
 						<th scope="col"><a href="index.php?edit=<?php echo $action['id']; ?>" class="rounded btn-sm btn-primary">Voir</a></th>
 
					</tr>

					<?php  
				} ?>
			</tbody>
		</table>
		<?php  
	}else{
	    $v->alertDanger($v->setError("Aucune donnee trouvée "));
 	}
 ?>


 


 
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bulma.min.js" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>