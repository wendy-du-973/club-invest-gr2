<?php

use OTPHP\TOTP;

/**
 * 
 */
class View
{
	private $r;

	protected $title;
	protected $error;
	protected $footer;

	private $menuAdmin = array(
		"Accueil" => " ?page=acceuil",
		"Inscription" => " ?page=inscription",
		"vote" => " ?page=vote",
		"Liste des Membres" => " ?page=listeMembre",
		"Forum" => " ?page=forum",
		"Envoyer une Info " => " ?page=sendInfo",
		"🌐 Se déconnecter" => " ?page=logout"
	);
	public function getMenuAdmin(){
		return $this->menuAdmin;
	}

	private $menuOffLine = array(
		"Accueil" => " ?page=acceuil",
		"Présentation" => " ?page=presentation",
		"✉ Contact" => " ?page=contact",
		"✉ Contact" => " ?page=contact",
		"✉ Contact" => " ?page=contact",
		"activeCompte" => " ?page=activeCompte",
		"🌐 Connexion" => " ?page=login"
	);

	private $menuUser = array(
		"Accueil" => " ?page=acceuil",
		// "Activer Compte" => " ?page=activeCompte",
		// "inscription" => " ?page=activeCompte",
		"Action" => " ?page=portefeuil",
		// "login" => " ?page=login",
		"Sondage" => " ?page=vote",
		"🌐 Se déconnecter" => " ?page=logout"
	);
	private $menuCom = array(
		"Accueil" => " ?page=acceuil",
		// "Activer Compte" => " ?page=activeCompte",
		// "inscription" => " ?page=activeCompte",
		"Action" => " ?page=portefeuil",
		"Forum" => " ?page=forum",
		"Ajouter une Action" => " ?page=addAction",
		"Sondage" => " ?page=vote",
		"Envoyer Newsletter" => " ?page=sendNewsletter",
		"🌐 Se déconnecter" => " ?page=logout"
	);
	private $menuCompta = array(
		"Accueil" => " ?page=acceuil",
		// "Activer Compte" => " ?page=activeCompte",
		// "inscription" => " ?page=activeCompte",
		"Présentation" => " ?page=presentation",
		"Ajouter une Action" => " ?page=addAction",
		"Forum" => " ?page=forum",
		"Sondage" => " ?page=vote",
		// "login" => " ?page=login",
		"🌐 Se déconnecter" => " ?page=logout"
	);


	function __construct($argument)
	{
		$this->r = $argument;
	}
	/*******************************/
	/* ****** GETTERS **************/
	/*******************************/
 		public function showHeader(){
		?>
		 
		<?php  
	}
	public function showMenuCom(){

		?>
		<nav class="navbar navbar-expand-lg navbar-light rounded" style="background-color:#DF1531;">
	<div class="container-fluid text-light">
	<a class="navbar-brand" href="#"><img class="rounded-circle" width="60" src="View/image/logo.png"></a>
	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarScroll">
		<ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
			
			<?php
			foreach ($this->menuCom as $texte => $lien) {
			
					echo "<li class='nav-item border-left'>";
					echo "<a class='nav-link text-light active' href=\"$lien\">$texte</a>";
					echo "</li>";
			
			} ?>

			
		</ul>
		
		</div>
	</div>
	</nav>


<?php
			}
			public function showMenuUser(){

				?>
			<nav class="navbar navbar-expand-lg navbar-light rounded" style="background-color:#DF1531;">
				<div class="container-fluid text-light">
				<a class="navbar-brand" href="#"><img class="rounded-circle" width="60" src="View/image/logo.png"></a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarScroll">
					<ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
						
						<?php
						foreach ($this->menuUser as $texte => $lien) {
						
								echo "<li class='nav-item border-left'>";
								echo "<a class='nav-link text-light active' href=\"$lien\">$texte</a>";
								echo "</li>";
						
						} ?>

			
		</ul>
		
		</div>
	</div>
	</nav>

<?php
					}
					public function showMenuOffLine()
					{
				
					?>	 
				<header class="container ">
  <!-- Navbar -->
  <nav class="navbar container navbar-expand-lg navbar-light bg-white fixed-top">
    <div class="container-fluid">
      <button
        class="navbar-toggler"
        type="button"
        data-mdb-toggle="collapse"
        data-mdb-target="#navbarExample01"
        aria-controls="navbarExample01"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarExample01">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        	<?php
			foreach ($this->menuOffLine as $texte => $lien) {
			
					echo "<li class='nav-item border-left'>";
					echo "<a class='nav-link  active' href=\"$lien\">$texte</a>";
					echo "</li>";
			
			} ?>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Navbar -->

  <!-- Background image -->
  <div
    class="p-5 text-center bg-image"
    style="
      background-image: url('View/image/iae2.jpg');
      height: 300px;
      margin-top: 58px;
    "
  >
    <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
      <div class="d-flex justify-content-center align-items-center h-100">
        <div class="text-white">
          <h1 class="mb-3">Heading</h1>
          <h4 class="mb-3">Subheading</h4>
          <a class="btn btn-outline-light btn-lg" href="#!" role="button"
          >Call to action</a
          >
        </div>
      </div>
    </div>
  </div>
  <!-- Background image -->
</header>	
				
						 
 							
						
				
				<?php
					}


					public function showMenuComPta()
					{
				
						?>
				<nav class="navbar navbar-expand-lg navbar-light rounded" style="background-color:#DF1531;">
					<div class="container-fluid text-light">
					<a class="navbar-brand" href="#"><img class="rounded-circle" width="60" src="View/image/logo.png"></a>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarScroll">
						<ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
							
							<?php
							foreach ($this->menuCompta as $texte => $lien) {
							
									echo "<li class='nav-item border-left'>";
									echo "<a class='nav-link text-light active' href=\"$lien\">$texte</a>";
									echo "</li>";
							
							} ?>
				
							
						</ul>
						
						</div>
					</div>
					</nav>
				
				<?php
					}
	public function showMenuAdmin(){
		?>
		 <div class="menuAdmin">
	      <ul>
	      	<?php
			foreach ($this->menuAdmin as $texte => $lien) {
			
					echo "<li class='nav-item '>";
					echo "<a class='ml-4 mt-3 mb-3 text-dark active' href=\"$lien\">$texte</a>";
					echo "</li>";
			
			} ?>
	       
	      </ul>
    </div>

		<?php  
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function getError()
	{
		return $this->error;
	}

	public function getFooter()
	{
		return $this->footer;
	}
	/*******************************/
	/* ****** SETTERS **************/
	/*******************************/

	public function setMenu($menu)
	{
		$this->menu = $menu;
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}
	public function setError($error)
	{
		$this->error = $error;
	}
	public function setFooter($footer)
	{
		$this->footer = $footer;
	}
	/*******************************/
	/* ****** CODE  **************/
	/*******************************/

	public function alertDanger($text)
	{
	?>
		<div class="alert alert-danger"><?php echo $this->getError(); ?></div>
	<?php
	}
	public function alertSuccess($text)
	{
	?>
		<div class="alert alert-success"><?php echo $this->getError(); ?></div>
	<?php
	}

	public function whoIsConneted($text)
	{
	?>
		<div class="container">

		</div>
	<?php
	}
	public function alertIntruision()
	{
		$text = "vous n'avez pas le dorit d'etre la ";
	?>
		<div class="jumbotron jumbotron-fluid bg-danger">
			<div class="container">
				<h1 class="display-4"> <?php echo $text; ?> </h1>
				<p class="lead">nous avons informer votre admin. </p>
			</div>
		</div>
	<?php
	}

	public function alertSimpleUser(){
		$this->alertDanger($this->setError("Access Denied"));
	}
	public function userIsLocked()
	{
	?>
		<div class="jumbotron jumbotron-fluid bg-danger">
			<div class="container">
				<h1 class="display-4">Vous n'avez pas le droit </h1>
				<p class="lead">Veuillez contacter votre admin. </p>
			</div>
		</div>
	<?php
	}

	/*******************************/
	/* ****** PAGE LINK **************/
	/*******************************/
	

	public function showAcceuil()
	{
		require_once('Pages/Acceuil.php');
	}
	public function showA2F($data)
	{

		$otp = TOTP::create('FNC7GMRXKHTHVLKHTRFRIAQ2D4YGN5PTNG3T3UWXYX4PFOOOZMAFUEOLW4JAHQK37T736JKFV3QG5UYC3Y7XVJ4BTUGAEZK7C4LLP7Q'); // New TOTP

		$otp->setLabel(' Club Investissement'); // The label (string)

		$chl = $otp->getProvisioningUri();
		$link = "https://chart.googleapis.com/chart?cht=qr&chs=300x300&&chl=" . $chl;
	?>
		<center>
			<h4>Authenification a deux facteur</h4>
			<p>Scanner le Qr code</p>
		</center>
		<div class="row">
			<div class="col-md-6">
				<form method="POST" action="index.php?page=<?php echo $this->r->a2f(); ?>">
					<?php if (array_key_exists('code', $data)) { ?>
						<input value="<?php echo $data['code']; ?>" type="text" name="code" class="form-control">
					<?php } else { ?>
						<input type="text" name="code" class="form-control">

					<?php } ?>
					<input type="submit" name="" value="Envoyer " class="btn btn-primary  mt-2 col-md-12">
				</form>
			</div>
			<div class="col-md-6">
				<img src="<?php echo $link; ?>">
			</div>
		</div>

	<?php
		// var_dump($data);
		// print_r($link);
	}

	public function liseVotes($data)
	{
		require_once('Pages/listeVotes.php');
	}
	public function HaveAllreadyVoted()
	{
	?>
		<div class="jumbotron jumbotron-fluid bg-success">
			<div class="container">
				<h1 class="display-4">Vous avez deja voté</h1>
				<p class="lead">vous ne pouvez voter qu'une fois pour un vote. Et vous ne pouvez pas revenir sur vote vote </p>
			</div>
		</div>
	<?php
	}
	public function getAllSubject(){
		?>

		<?php  
	}
	public function canNotUserVote()
	{
	?>
		<span class="badge badge-danger">vous n'avez pas le doit de voter </span>

	<?php
	}
	public function commentThisSujet($sujet)
	{
	?>
		<form method="post" action="index.php?page=<?php echo $this->r->commentThisSujet(); ?>">
			<input type="hidden" name="idUser" value="<?php echo $this->r->getAuth()->getUserName()['id']; ?>">
			<input type="hidden" name="idSujet" value="<?php echo $sujet['id']; ?>">
			<input type="text" name="comment" class="form-control" placeholder="votre commenraire">
			<input type="submit" name="" value="Commenter le sujet">
		</form>
	<?php
	}
	 public function getSujet($sujet)
	{
	?>
		<div class="card">
			<div class="card-header" id="headingOne">
				<?php if ($sujet['status'] == 'terminee') : ?>
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<?php echo $sujet['sujet']; ?>
						</button>
					</h5>
				<?php else : ?>
					<div class="col-md-8">
						<h5 class="mb-3 ">
							<button class="btn btn-link text-white" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								<?php echo $sujet['sujet']; ?>
							</button>
						</h5>
					</div>
					<div class="col-md-4">
						<?php if ($sujet['idUser'] === $this->r->getAuth()->getUserName()['id']) : ?>
							<?php $this->termineeSujet($sujet['idUser'], $sujet['id']); ?>
							<button>supprimer</button>
						<?php else : ?>
							<h5>cest pa sa toi </h5>
						<?php endif ?>
					</div>
				<?php endif ?>

			</div>


		<?php
	}
	public function getCommentsForSujet($commentaire)
	{ 
		// var_dump($commentaire);
		$userRequest= $this->r->getController()->getUserRequest();
		
		?>
		<!-- <ul> -->
			<!-- <li> -->
				<?php foreach ($commentaire as $com ): ?>
					<div class="row mb-3 mt-3">
			      		<div class="col-md-2 rounded  "> <?php 
			      			echo $userRequest->findUserById($com['idUser'])['prenom'];
			      		 ?></div>
			      		<div class="col-md-10 rounded   commentText">
			      			<?php echo $com['commentaire']; ?>
			      		</div>
			      	</div> 
				<?php endforeach ?>
			<!-- </li> -->
		<!-- </ul> -->
			
			<!-- <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
				<div class="card-body">
					<?php echo $commenraire['commentaire']; ?>
				</div>
			</div>
		</div> -->
	<?php
	}
	public function termineeSujet($idUser, $id)
	{
	?>
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
			Terminee
		</button>
		<form method="POST" action="index.php?page=<?php echo $this->r->termineeSujet(); ?>">
			<input type="hidden" name="idUser" value="<?php echo $idUser; ?>">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Etes Vous Surs ?</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
							<input type="submit" class="btn btn-success" name="" value="Terminee">
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php
	}
	public function showDetailAction($data){
		require_once('Pages/DetailAction.php');
	}
	public function addAction($data){
		require_once('Pages/addAction.php');
	}
	public function dashboardPage()
	{
		require_once('Pages/admin.php');
	}
	public function showPresentation()
	{
		require_once('Pages/Presentation.php');
	}
 
	public function showFooterOfLine()
	{
		require_once('Pages/Footer.php');
	}
	public function showContactForm($data)
	{
		require_once('Pages/Contact.php');
	}
	public function showLogin($data)
	{
		require_once('Pages/Login.php');
	}
	public function showAdmin()
	{
		require_once('Pages/admin.php');
	}

	public function showNewsLetterForm()
	{
		require_once('Pages/NewsLetter.php');
	}

	public function getPortefeuil()
	{
		require_once('Pages/Portefeuil.php');
	}

	public function getAchat()
	{
		require_once('Pages/Achat.php');
	}

	public function getVote($data)
	{
		// if (!$data) {
		// $data=null;
		// }
		require_once('Pages/vote.php');
	}

	public function getInscription($data)
	{
		require_once('Pages/Inscription.php');
	}

	public function logout()
	{
		require_once('Pages/LogOut.php');
	}

	public function activeCompte($data)
	{
		require_once('Pages/ActiveCompte.php');
	}

	public function fogot($data)
	{
		require_once('Pages/Fogot.php');
	}

	public function reinitialize($data)
	{
		require_once('Pages/reinitialize.php');
	}

	public function sendNewsletter($data)
	{
		require_once('Pages/sendNewsletter.php');
	}
	public function listeMembre()
	{
		require_once('Pages/ListeMembre.php');
	}
	public function sendInfo($data)
	{
		require_once('Pages/SendInfo.php');
	}

	public function showAllTchat()
	{
		require_once('Pages/Tchat.php');
	}

	public function showForum()
	{
		require_once("Pages/Forum.php");
	}


	public function desineAdminDashboard()
	{
		require_once('Pages/admin.php');
	}
}
