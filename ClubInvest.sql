-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 04 jan. 2022 à 03:49
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ClubInvest`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `commentaire` text NOT NULL,
  `idSujet` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `commentaire`, `idSujet`, `idUser`) VALUES
(1, '\r\n    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 2),
(2, '\r\n    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo', 1, 2),
(3, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.', 1, 2),
(4, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.', 2, 2),
(5, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.', 1, 2),
(6, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.', 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `date_env` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `email`, `contenu`, `date_env`) VALUES
(1, 'bahibrahimasory09@gmail.com', 'je trouve que vous faites du bon boulot            \r\n        ', '2021-12-18 21:36:04'),
(2, 'bahibrahimasory09@gmail.com', 'je trouve que vous faites du bon boulot            \r\n        ', '2021-12-18 21:36:30'),
(3, 'bahibrahimasory09@gmail.com', 'je trouve que vous faites du bon boulot            \r\n        ', '2021-12-18 21:37:03');

-- --------------------------------------------------------

--
-- Structure de la table `inscriptionCode`
--

CREATE TABLE `inscriptionCode` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `code` int(6) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `inscriptionCode`
--

INSERT INTO `inscriptionCode` (`id`, `email`, `code`, `createdAt`, `role`) VALUES
(1, 'bahibrahimasory09@gmail.com', 456845, '2021-12-15 01:00:09', 1),
(2, 'bahibrahimasory09@gmail.com', 6978799, '2021-12-15 01:00:13', 2);

-- --------------------------------------------------------

--
-- Structure de la table `NewsLetter`
--

CREATE TABLE `NewsLetter` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `status` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `NewsLetter`
--

INSERT INTO `NewsLetter` (`id`, `titre`, `contenu`, `status`) VALUES
(1, 'rendez-vous avec les informaticiens le 15 janvier', 'j\'aimerai que vous soyez tous présent a ce Rendezvous			 		\r\n			 	', 'private'),
(2, '0 sécurité pour le moment sur mes donnes', 'je ferrai plumard cela en attendant je mets en place un système qui n\'est pas sécurise mais qui marche			 		\r\n			 	', 'private'),
(3, '0 sécurité pour le moment sur mes donnes', 'je ferrai plumard cela en attendant je mets en place un système qui n\'est pas sécurise mais qui marche			 		\r\n			 	', 'private'),
(4, '0 sécurité pour le moment sur mes donnes', 'je ferrai plumard cela en attendant je mets en place un système qui n\'est pas sécurise mais qui marche			 		\r\n			 	', 'private'),
(5, '0 sécurité pour le moment sur mes donnes', 'je ferrai plumard cela en attendant je mets en place un système qui n\'est pas sécurise mais qui marche			 		\r\n			 	', 'private');

-- --------------------------------------------------------

--
-- Structure de la table `sujet`
--

CREATE TABLE `sujet` (
  `id` int(11) NOT NULL,
  `sujet` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `idUser` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `sujet`
--

INSERT INTO `sujet` (`id`, `sujet`, `status`, `idUser`, `createdAt`) VALUES
(1, 'vous avez vu le nouveau AQUAMAN ?', 'encours', 2, '2022-01-04 02:26:25'),
(2, 'j\'arrête pas de penser a la bouffe dans mon congélateur', 'encours', 2, '2022-01-04 02:26:25'),
(3, 'la relance économique des états de l\'Afrique de l\'ouest', 'encours', 2, '2022-01-04 02:26:25'),
(4, 'le vote qui va passer au sena a propos du passe vaccinal', 'encours', 2, '2022-01-04 02:26:25'),
(5, 'nouvelle source de revenue', 'encours', 2, '2022-01-04 02:26:25'),
(6, 'nouvelle source de revenue', 'encours', 2, '2022-01-04 02:26:25'),
(8, 'il faut que j\'envoie ce que j\'ai fait jusque la meme si je n\'ai pas eu le temps de travailler dessus ce soir', 'encours', 2, '2022-01-04 02:28:21'),
(9, 'il faut que j\'envoie ce que j\'ai fait jusque la meme si je n\'ai pas eu le temps de travailler dessus ce soir', 'encours', 2, '2022-01-04 02:30:24'),
(10, 'il faut que j\'envoie ce que j\'ai fait jusque la meme si je n\'ai pas eu le temps de travailler dessus ce soir', 'encours', 2, '2022-01-04 02:30:50'),
(11, 'tu aurais du travailler plus dessus au lieu de faire autre chose', 'encours', 1, '2022-01-04 02:32:57'),
(12, 'tu aurais du travailler plus dessus au lieu de faire autre chose', 'encours', 1, '2022-01-04 02:41:27');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `tel` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `password`, `status`, `role`, `tel`) VALUES
(1, 'bah', 'ib', 'ib', 'toto', 'none', 'admin', 0),
(2, 'sim', 'aboubacr', 'sim', 'titi', 'none', 'com', 0),
(10, 'SIM', 'ABOU', 'novaanglo8@gmail.com', NULL, 'unlock', 'simple', 0),
(11, 'wendy', 'alphanor', 'wendy.alphanor@outlook.fr', NULL, 'lock', 'com', 0),
(12, NULL, NULL, 'bahibrahimasory09@gmail.com', NULL, 'unlock', 'compta', 0),
(13, NULL, NULL, 'bahibrahimasory09@gmail.com', NULL, NULL, 'simple', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Votant`
--

CREATE TABLE `Votant` (
  `id` int(11) NOT NULL,
  `idVote` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE `vote` (
  `id` int(11) NOT NULL,
  `sujet` varchar(255) NOT NULL,
  `duree` int(2) NOT NULL,
  `status` varchar(10) NOT NULL,
  `oui` int(2) DEFAULT NULL,
  `non` int(2) DEFAULT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vote`
--

INSERT INTO `vote` (`id`, `sujet`, `duree`, `status`, `oui`, `non`, `idUser`) VALUES
(1, 'achter du BabyYooshi', 30, 'terminee', NULL, NULL, 0),
(2, 'achter du BabyYooshi', 30, 'terminee', NULL, NULL, 0),
(3, 'achter du BabyYooshi', 30, 'terminee', NULL, NULL, 0),
(4, 'achter du BabyYooshi', 30, 'terminee', NULL, NULL, 0),
(5, 'achter du BabyYooshi', 30, 'terminee', NULL, NULL, 0),
(6, 'achter du BabyYooshi', 30, 'terminee', NULL, NULL, 0),
(7, 'vendre nos action chez AIR FRANCE', 45, 'terminee', NULL, NULL, 2),
(8, 'acheter des Elon Doge', 45, 'encours', NULL, NULL, 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `inscriptionCode`
--
ALTER TABLE `inscriptionCode`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `NewsLetter`
--
ALTER TABLE `NewsLetter`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sujet`
--
ALTER TABLE `sujet`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Votant`
--
ALTER TABLE `Votant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `inscriptionCode`
--
ALTER TABLE `inscriptionCode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `NewsLetter`
--
ALTER TABLE `NewsLetter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `sujet`
--
ALTER TABLE `sujet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `Votant`
--
ALTER TABLE `Votant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
