<?php 
require_once('db.php');
 /**
  * 
  */

 class ActionRequest 
 {
 	private $db;
 	function __construct()
 	{
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
 	}
 	public function getAction($id){
 		$query = 'SELECT  *
				  FROM action 
				  WHERE id = :id 
				  OR email = :id ;' ;

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id));
		$rest = $st->fetch();
		
		/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
		if (!$rest)
			return null;

		/* on construit l'utilisateur à partir des informations récupérées */
		return new action($rest['nom'], $rest['email'], $rest['password']);
 	}

  
 	public function findUserByName($name){
 		$query = "SELECT * FROM action where name=:name  ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':name' => $name) );
		$result = $st->fetch();
 			return $result;
 	}
 
 	public function getAllAction(){
	 $stmt = $this->db->query("SELECT * FROM action ");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;

	}
		public function newAction($nom,$prixActu,$prixAchat	){
 		 $query = 'INSERT INTO action (nom, prixActu,prixAchat) 
		VALUES (:nom ,:prixActu,:prixAchat);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':nom' => $nom,
			':prixActu' => $prixActu,
			':prixAchat' => $prixAchat
		));

 	}
 	 

  
 	public function findActionById($id){
 		$query = "SELECT * FROM action where id=:id  ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id) );
		$result = $st->fetch();
 			return $result;

 	}
 	public function isDoublon($data){
	 		$query = "SELECT * FROM action where nom=:nom and prixActu=:prixActu ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':nom' => $data['nom'],
			'prixActu'=>$data['prixActu'],
 		) );
		$result = $st->fetch();
		if ($result!=null)
			return $result;
		return $this->newAction($data['nom'],$data['prixActu'],$data['prixAchat']);
 	}
 	public function setAction($data){

 			$rq = "UPDATE action SET nom=:nom,prixActu=:prixActu,prixAchat=:prixAchat
			 WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' => $data['id'],
			  ':nom' => $data['nom'],
			  ':prixActu' => $data['prixActu'],
			  ':prixAchat' => $data['prixAchat']
			);
			
	$resutl =$stmt->execute($data);
	return $resutl;
 	}
 // 	public function inscritptionCode($email,$code){
 // 		 $query = 'INSERT INTO inscriptionCode (email, code) 
	// 	VALUES (:email ,:code);';

	// 	$st = $this->db->prepare($query);
	// 	return $st->execute(array(
	// 		':email' => $email,
	// 		':code' => $code
	// 	));
 // 	}
 // 	public function activateAccount($data){
 // 			$rq = "UPDATE action SET status=:status 
	// 		 WHERE email=:email ;" ;
	// 		$stmt = $this->db->prepare($rq);
	// 		$data = array(
	// 		  ':email' => $data['email'],
	// 		  ':status' => "unlock",
	// 		  ':password'=>$data['password']
	// 		);
			
	// $resutl =$stmt->execute($data);
	// return $resutl;
 // 	}

 // 	public function sendCodeToResetPassword($email,$code){
 // 		 $query = 'INSERT INTO inscriptionCode (email, code,role) 
	// 	VALUES (:email ,:code,:role);';
	// 	$role="reset";
	// 	$st = $this->db->prepare($query);
	// 	return $st->execute(array(
	// 		':email' => $email,
	// 		':code' => $code,
	// 		':role' => $role
	// 	));
 // 	}

 // 	public function findEmailAndCodeToResetPassword($data){
 // 		 		$query = "SELECT * FROM inscriptionCode where email=:email and code=:code and role=:role  ;";

	// 	$st = $this->db->prepare($query);
	// 	$st->execute(array(':email' => $data['email'],':code'=>$data['code'], ':role'=>'reset') );
	// 	$result = $st->fetch();
 // 			return $result;	
 // 	}
 // 	public function resetPassword($data){
 // 		$rq = "UPDATE action SET password=:password 
	// 		 WHERE email=:email ;" ;
	// 		$stmt = $this->db->prepare($rq);
	// 		$data = array(
	// 		  ':email' => $data['email'],
	// 		  ':password' => $data['password']
	// 		);
			
	// $resutl =$stmt->execute($data);
	// return $resutl;
 // 	}

 	public function deleteAction($data){
 		$rq = "DELETE FROM action WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' => $data['id']
			);
			
	$resutl =$stmt->execute($data);
	return $resutl;
 	}

 // 	public function getExternalUserEmail(){
 // 		 $stmt = $this->db->query("SELECT email FROM action where role = 'simple' ");
	// 	$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// 	return $tableau;
 // 	}
 }