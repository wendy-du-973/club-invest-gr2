<?php 
require_once('db.php');
/**
 * 
 */
class inscriptionCodeRequest 
{
	protected $db;
	function __construct()
	{
		
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
	}
	public function findCode($email){
 		$query = "SELECT * FROM inscriptionCode where email=:email ORDER BY id desc LIMIT 1 ;";
		$st = $this->db->prepare($query);
		$st->execute(array(':email' => $email) );
		$result = $st->fetch();
 			return $result;

	}
} 