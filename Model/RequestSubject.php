 <?php 
 require_once('db.php');

 class RequestSubject{
	 	 

 	private $db;
 	private $NewsBuilder;
 	function __construct()
 	{
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
 	}
 	public function getNews($id){
 		$query = 'SELECT  *
				  FROM News 
				  WHERE id = :id 
				  ' ;

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id));
		$rest = $st->fetchAll(PDO::FETCH_ASSOC);
		
		/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
		if (!$rest)
			return null;

		/* on construit l'utilisateur à partir des informations récupérées */
		return new News($rest['titre'], $rest['contenu']);
 	}
 	public function newSujet($data){
 		 $query = 'INSERT INTO sujet (sujet, status,idUser) 
		VALUES (:sujet ,:status,:idUser);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':sujet' => $data['sujet'],
			':status' => 'encours',
			':idUser'=>$data['idUser']

		));
 	}
 		public function findAllSujet(){
		$stmt = $this->db->query("SELECT * FROM sujet order by id desc");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;

 	}

 	public function endSujet($data){
 		$rq = "DELETE FROM  sujet   WHERE id=:id ";
		$stmt = $this->db->prepare($rq);
		 		$data = array(
			  ':id' => $data['id']
				);

		$stmt->execute($data);
		return $stmt->rowCount();
 	}



}
?>