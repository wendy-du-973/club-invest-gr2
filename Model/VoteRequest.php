<?php 
require_once('db.php');
 /**
  * 
  */

 class VoteRequest 
 {
 	private $db;
 	private $NewsBuilder;
 	function __construct()
 	{
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
 	}
 	public function getConact($id){
 		$query = 'SELECT  *
				  FROM contact 
				  WHERE id = :id 
				  ' ;

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id));
		$rest = $st->fetch();
		
		/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
		if (!$rest)
			return null;

		/* on construit l'utilisateur à partir des informations récupérées */
		return new contact($rest['titre'], $rest['contenu']);
 	}
 	public function newVote($data){
 		 $query = 'INSERT INTO vote (sujet, idUser,status,duree) 
		VALUES (:sujet ,:idUser,:status,:duree);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':sujet' => $data['sujet'],
			':idUser' => $data['idUser'],
			':status' => "encours",
			':duree' => $data['duree']

		));
 	}
 	public function isVoteDoing(){
 		$query = "SELECT * FROM vote where status=:status  ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':status' => "encours") );
		$result = $st->fetch();
 			return $result;
 	}
	public function endVotes(){
		$sql = "DELETE FROM vote WHERE status = :status ";
		return $this->db->prepare($sql)->execute(array(':status' => "encours"));
	}

	public function findAllVotes(){
			$stmt = $this->db->query("SELECT * FROM vote order by id desc");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;

	}
 
 	public function upgradeVote($data){
 		if (strcmp($data['choix'],'oui')==0) {
	 		echo $this->isVoteDoing()['oui']+1 ; 
 				$rq = "UPDATE vote SET oui=:oui 
			 WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' =>  $this->isVoteDoing()['id'],
			  ':oui' => $this->isVoteDoing()['oui']+1
			);
			$resutl =$stmt->execute($data);
			return $resutl; 

 		}
 		if (strcmp($data['choix'],'non')==0) {
 			echo $this->isVoteDoing()['oui']+1 ; 
 				$rq = "UPDATE vote SET non=:non 
			 WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' =>  $this->isVoteDoing()['id'],
			  ':non' => $this->isVoteDoing()['non']+1
			);
			$resutl =$stmt->execute($data);
			return $resutl; 
 		}

}
 public function inscritVotant($id){
 	 $query = 'INSERT INTO votant (idUser) 
		VALUES (:id);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':id' => $id
		));
}
public function hasAllreadyVoted($idUser){
	$query = "SELECT * FROM votant where idUser=:idUser  ;";

	$st = $this->db->prepare($query);
	$st->execute(array(':idUser' => $idUser) );
	$result = $st->fetch();
	return $result;
}	

 public function nbVotant(){
 		$stmt = $this->db->query("SELECT count(*) FROM votant ");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $tableau;
 }
 public function ubuserCanVote(){
 		$stmt = $this->db->query("SELECT count(*) FROM user WHERE status!='lock' AND role!='simple' ");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;
 }
 public function canUservote(){

 }
 
}