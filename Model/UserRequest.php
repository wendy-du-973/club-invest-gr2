<?php 
require_once('db.php');
 /**
  * 
  */

 class UserRequest 
 {
 	private $db;
 	function __construct()
 	{
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
 	}
 	public function getUser($id){
 		$query = 'SELECT  *
				  FROM user 
				  WHERE id = :id 
				  OR email = :id ;' ;

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id));
		$rest = $st->fetch();
		
		/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
		if (!$rest)
			return null;

		/* on construit l'utilisateur à partir des informations récupérées */
		return new User($rest['nom'], $rest['email'], $rest['password']);
 	}

 	public function checkAuth($email,$password){
 		try {
 			$query = "SELECT * FROM user where email=:email and password=:password ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':email' => $email,':password'=>$password));
		$result = $st->fetch();
			return $result;
		} catch (Exception $e) {
			print $e->getMessage();
		}
 	}
 	public function findUserByEmail($email){
 		$query = "SELECT * FROM user where email=:email  ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':email' => $email) );
		$result = $st->fetch();
 			return $result;
 	}
 	public function getUserProfile($data){
 		/*** préparation ***/
			$rq = "SELECT * FROM user WHERE email = : email ";
			$stmt = $db->prepare($rq);
			/*** remplissage des paramètres ***/
			$data = array(":email" => "email");
			/*** exécution du statement ***/
			$stmt->execute($data);
			/*** récupération du résultat ***/
			$result = $stmt->fetchAll();
			return $result;
		}

	  public function activeCompte($email)
	{
			$rq = "UPDATE user SET status='unlock'  WHERE email=:email";
 		$stmt = $this->db->prepare($rq);
 		$data = array(
		  ':email' => $email
		);

		$stmt->execute($data);


		return $stmt->rowCount();
	}
	public function getAllEmail(){
	 $stmt = $this->db->query("SELECT email FROM user ");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;

	}
		public function inscriptionUser($email){
 		 $query = 'INSERT INTO user (email, role,status,nom,prenom) 
		VALUES (:email ,:role,:status,:nom,:prenom);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':email' => $email,
			':role' => "simple",
			':status' => "none",
			':nom' => "",
			':prenom' => "",
		));

 	}
 	public function newUser($data){
 		 $query = 'INSERT INTO user (email, role,status,nom,prenom) 
		VALUES (:email ,:role,:status,:nom,:prenom);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':email' => $data['email'],
			':role' => $data['role'],
			':status' => "lock",
			':nom' => $data['nom'],
			':prenom' => $data['prenom']
		));
 	}

 	public function findAllUsers(){
		$stmt = $this->db->query("SELECT * FROM user order by id desc");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;

 	}
 	public function findUserById($id){
 		$query = "SELECT * FROM user where id=:id  ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id) );
		$result = $st->fetch();
 			return $result;

 	}
 	public function lockOrUnlock($user){
 		if($user['status']==="lock"){
 				$rq = "UPDATE user SET status=:status 
			 WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' => $user['id'],
			  ':status' => "unlock"
			);

 		}else {
		 	$rq = "UPDATE user SET status=:status 
			 WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' => $user['id'],
			  ':status' => "lock"
			);
 		}

	$resutl =$stmt->execute($data);
	return $resutl;
 	}

 	public function setRole($data){
 			$rq = "UPDATE user SET role=:role 
			 WHERE id=:id ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':id' => $data['id'],
			  ':role' => $data['role']
			);
			
	$resutl =$stmt->execute($data);
	return $resutl;
 	}
 	public function inscritptionCode($email,$code){
 		 $query = 'INSERT INTO inscriptionCode (email, code) 
		VALUES (:email ,:code);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':email' => $email,
			':code' => $code
		));
 	}
 	public function activateAccount($data){
 			$rq = "UPDATE user SET status=:status 
			 WHERE email=:email ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':email' => $data['email'],
			  ':status' => "unlock",
			  ':password'=>$data['password']
			);
			
	$resutl =$stmt->execute($data);
	return $resutl;
 	}

 	public function sendCodeToResetPassword($email,$code){
 		 $query = 'INSERT INTO inscriptionCode (email, code,role) 
		VALUES (:email ,:code,:role);';
		$role="reset";
		$st = $this->db->prepare($query);
		return $st->execute(array(
			':email' => $email,
			':code' => $code,
			':role' => $role
		));
 	}

 	public function findEmailAndCodeToResetPassword($data){
 		 		$query = "SELECT * FROM inscriptionCode where email=:email and code=:code and role=:role  ;";

		$st = $this->db->prepare($query);
		$st->execute(array(':email' => $data['email'],':code'=>$data['code'], ':role'=>'reset') );
		$result = $st->fetch();
 			return $result;	
 	}
 	public function resetPassword($data){
 		$rq = "UPDATE user SET password=:password 
			 WHERE email=:email ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':email' => $data['email'],
			  ':password' => $data['password']
			);
			
	$resutl =$stmt->execute($data);
	return $resutl;
 	}

 	public function deleteDataResetPassword($data){
 		$rq = "DELETE FROM inscriptionCode WHERE email=:email and code=:code ;" ;
			$stmt = $this->db->prepare($rq);
			$data = array(
			  ':email' => $data['email'],
			  ':code' => $data['code']
			);
			
	$resutl =$stmt->execute($data);
	return $resutl;
 	}

 	public function getExternalUserEmail(){
 		 $stmt = $this->db->query("SELECT email FROM user where role = 'simple' ");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;
 	}
 }