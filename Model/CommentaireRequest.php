<?php 
require_once('db.php');
 /**
  * 
  */

 class CommentaireRequest 
 {
 	private $db;
 	private $NewsBuilder;

 	function __construct()
 	{
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
 	}
 	public function getCommentsForSujet($id){
 		$query = 'SELECT  *
				  FROM Commentaire 
				  WHERE idSujet = :id 
				  ' ;
		$st = $this->db->prepare($query); 
		$st->execute(array(':id' => $id));
		$rest = $st->fetchAll(PDO::FETCH_ASSOC);
		
		/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
		// if (!$rest)
			return $rest;

		/* on construit l'utilisateur à partir des informations récupérées */
		// return new News($rest['titre'], $rest['contenu']);
 	}
 	public function commentAsubject($data){
 		 $query = 'INSERT INTO commentaire (commentaire, idSujet,idUser) 
		VALUES (:commentaire ,:idSujet,:idUser);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':commentaire' => $data['commentaire'],
			':idSujet' => $data['idSujet'],
			':idUser'=>$data['idUser']

		));
 	}
 	// public function sendRequestToEveryone($data){
 	// 	 $query = 'INSERT INTO NewsLetter (titre, contenu,status) 
		// VALUES (:titre ,:contenu,:status);';

		// $st = $this->db->prepare($query);
		// return $st->execute(array(
		// 	':titre' => $data['titre'],
		// 	':contenu' => $data['contenu'],
		// 	':status' => "private"

		// ));
 	// }


 
}