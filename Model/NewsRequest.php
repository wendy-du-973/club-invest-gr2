<?php 
require_once('db.php');
 /**
  * 
  */

 class NewsRequest 
 {
 	private $db;
 	private $NewsBuilder;
 	function __construct()
 	{
 			$myDataBase= new db();
		$this->db =  $myDataBase->getdb();
 	}
 	public function getNews($id){
 		$query = 'SELECT  *
				  FROM News 
				  WHERE id = :id 
				  ' ;

		$st = $this->db->prepare($query);
		$st->execute(array(':id' => $id));
		$rest = $st->fetch();
		
		/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
		if (!$rest)
			return null;

		/* on construit l'utilisateur à partir des informations récupérées */
		return new News($rest['titre'], $rest['contenu']);
 	}
 	public function sendRequestToEveryone($data){
 		 $query = 'INSERT INTO NewsLetter (titre, contenu,status) 
		VALUES (:titre ,:contenu,:status);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':titre' => $data['titre'],
			':contenu' => $data['contenu'],
			':status' => "private"

		));
 	}
 	public function sendNewsletter($data){
 		 $query = 'INSERT INTO NewsLetter (titre, contenu,status) 
		VALUES (:titre ,:contenu,:status);';

		$st = $this->db->prepare($query);
		return $st->execute(array(
			':titre' => $data['titre'],
			':contenu' => $data['contenu'],
			':status' => "public"

		));
 	}

 	public function getAllNewsletter(){
 $stmt = $this->db->query("SELECT * FROM NewsLetter ");
		$tableau = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tableau;
 	}
 	
 	

  
}