<?php
require_once('db.php');
require_once('UserRequest.php');
 
class AuthenticationManager
{

	protected $UserRequest;

	public function __construct($UserRequest)
	{
		 $this->UserRequest=$UserRequest;
 	}
			 
	public function  connectUser(String $email, String $password)
	{
		// echo "encours de conexion";
 		  if(($this->UserRequest->checkAuth($email,$password)) ){
				$_SESSION['user']=$this->UserRequest->findUserByEmail($email);
			return true;
		} else {
			return false;
		}
	}
	public function thereIsNoSession(){
		return (($_SESSION==null) or !($_SESSION));
		
	}

	public function   isUserConnected(){
		return (array_key_exists('user', $_SESSION)) ;
	}
	 
	public function   getUserName()
	{		if($_SESSION)
	 			return $_SESSION['user'];
	 		else
	 			return null;

	}
	// public function findUserByEmail(){
		// return $this->UserRequest->getUserByEmail($this->UserRequest->getUserName());
	// }

	public function getUserId(){
		if($this->isUserConnected()){
			return $this;
		}else{
			return null;
		}
	}
	public function getUserRole(){
		return $this->getUserName()['role'];
	}
	public function isAdminConnected(){
		$var1=$this->getUserRole();
		$var2="admin";
		return  !strcmp($var1, $var2);
	}


 public function getConetedUserID($email){
		$query = 'SELECT  id
				  FROM user 
				  WHERE email = :email 
				  ;';

		$st = $this->db->prepare($query);
		$st->execute(array(':email' => $email));
		$arr = $st->fetch();	
	}
	public function getUserStatus(){
		return $this->getUserName()['status'];
	}
 

	public function isComConnected(){
		  $var1=$this->getUserRole();
		  $var2="com";
		return  !strcmp($var1, $var2);
	}
	public function isComptaConnected(){
		  $var1=$this->getUserRole();
		  $var2="compta";
		return  !strcmp($var1, $var2);
	}
	public function isNotActive(){
		$var1=$this->getUserStatus();
		$var2="active";
		return  strcmp($var1, $var2);	
	}
 
	 
	public function disconnectUser()
	{
		unset($_SESSION);
		session_reset();
		session_destroy();

	}

}
