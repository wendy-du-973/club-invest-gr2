<?php 
require_once('db.php');
require_once('UserRequest.php');
  /**
  * 
  */
 class UserBuilder 
 {
 		protected $User;
  	protected $error;
  	
 	function __construct($User)
 	{
 		// if(!isset($User['nom'])) $User['nom'] ='';
 		if(!isset($User['email'])) $User['email'] ='';
 		if(!isset($User['password'])) $User['password'] ='';
 		if(!isset($User['nom'])) $User['nom'] ='';
 		if(!isset($User['prenom'])) $User['prenom'] ='';
 		if(!isset($User['role'])) $User['role'] ='';
 		if(!isset($User['code'])) $User['code'] ='';
 		if(!isset($User['conf_password'])) $User['conf_password'] ='';
		$this->error = '';  
		$this->User=$User;
		$this->User['email']=$User['email'];
		$this->User['password']=$User['password'];
		$this->User['nom']=$User['nom'];
		$this->User['prenom']=$User['prenom'];
		$this->User['role']=$User['password'];
		$this->User['conf_password']=$User['conf_password'];
		// var_dump($User);
 	
 	}
 	/**
	 * Renvoie le tableau de données.
	 */
	public function getUser() {
		return $this->User; 
	}

	// /**
	//  * Renvoie les erreurs sur les données courantes,
 // 	 */
	public function getError() {
		return $this->error;
	} 	 
	public function getEmail() {
		return htmlspecialchars(stripslashes($this->User['email']));
	} 	 
	public function getNom() {
		return htmlspecialchars(stripslashes($this->User['nom']));
	} 	 
	public function getPrenom() {
		return htmlspecialchars(stripslashes($this->User['prenom']));
	} 	 
	public function getRole() {
		return htmlspecialchars(stripslashes($this->User['role']));
	} 	 
	public function getCode() {
		return htmlspecialchars(stripslashes($this->User['code']));
	} 	 

	public function getPassword() {
		return htmlspecialchars(trim($this->User['password']));
	}

	public function getConfPassword() {
		return htmlspecialchars(trim($this->User['conf_password']));
	}

	public function setError($value){
		$this->error=$value;
	}
 
 public function valideInscriptionUser($data){
	 	if (empty($this->getEmail())) {
	 		$this->setError("email obligatoire");	
	 	}

		if (!filter_var($this->getEmail(),FILTER_VALIDATE_EMAIL)) {
				$this->setError("adres email incorect ") ;
		}
		// __nom___
		if (empty($this->getNom())) {
	 		$this->setError("nom obligatoire");	
	 	}
	 	// __prenom___
		if (empty($this->getPrenom())) {
	 		$this->setError("prenom obligatoire");	
	 	}

	 	// _____role ____
		// if (empty($this->getRole())) {
	 		// $this->setError("Role obligatoire");	
	 	// }
	 	return $this->getError();
 

 }
  public function isValidResetPassword($data){
  	$this->User=$data;
		if (!filter_var($this->getEmail(),FILTER_VALIDATE_EMAIL)) {
				$this->setError("adres email incorect ") ;
		}
	 	if (empty($this->getEmail())) {
	 		$this->setError("email obligatoire");	
	 	}
	return $this->getError();

 }
	public function createUser() {
		// if($this->isValidLogin($this->User))
			// return new User($this->User['email'], $this->User['nom'], $this->User['password']);
		// return $this->error;
	}
	public function validLogin($data){
		($this->User['password']=$data['password']);
		// $this->getEmail()=$data['email'];
		// $this->getPassword()=$data['password'];
		if (!filter_var($this->getEmail(),FILTER_VALIDATE_EMAIL)) {
			$this->setError("adres email incorect ") ;
		}
		if (strlen($this->getPassword())<8  ) {
			$this->setError("le mot de pass doit etre superieur a 8 carractere ") ;
		}
		
		return $this->getError();
	}


	public function logUser(){
		return ($this->isValidLogin());			
	}
	public function connectUser(){
		if($this->logUser()==''){
			 $UserRequest = new UserRequest();
			 $AuthenticationManager = new AuthenticationManager($UserRequest);
			  ($AuthenticationManager->connectUser($this->getEmail(),$this->getPassword()) );
				$_SESSION['user'] = $this->getEmail();
			 	header('location:index.php');

		}
	}
	// /**
	//  * Vérifie que les données envoyees par l'utilisateur sont valides.
	//  */
	public function tailleDesDonnes($data){
		if($this->User['email']=='' || $this->User['password']==''){
			$this->setError("vous devez remplir tous les champs avant vous conecter");
			  $this->User['email'];
			  $this->User['password'];
		} 
		if ($data === '' ) {
			$this->setError( "Vous devez rensigner tous champ ");
			 $this->error;
		}else if(strlen($data)>30){
			$this->setError ( "on vous a pas demander un discours ");
		}
		return $this->error;
	}
	public function isValidLogin() {
		

	 if	($this->tailleDesDonnes($this->User['email']))
	 	    $this->setError($this->tailleDesDonnes($this->User['email']));
	 	if($this->tailleDesDonnes($this->User['password']))
	 	    $this->setError($this->tailleDesDonnes($this->User['password']));


		return $this->error;
	}

	public function isvalidData(){
		// return filter_var($this->User->getEmail(),FILTER_VALIDATE_EMAIL);
		return true;
	}
	public function actviationCompteValidation($data){
		$this->User = $data;

		if($this->User['email']==''){
				 $this->setError("l'email ne peut pas etre vide ");
		}
		if (!filter_var($data['email'],FILTER_VALIDATE_EMAIL)) {
			$this->setError("adres email incorect ") ;
		}
		if($this->User['code']==''){
				 $this->setError("le code ne peut pas etre vide ");
		}
		if($this->User['password']==''){
				 $this->setError("le mot de pass ne peut pas etre vide ");

		}
		if( strlen($this->User['password']) >8  or  strlen(($this->User['password']) <4 )){
		  	 $this->setError("le mot de pass est entre 4 et 8 caractere");

  	}


		if( strlen($this->User['conf_password']) >8  or  strlen(($this->User['conf_password']) <4 )){
		  	 $this->setError("le mot de pass de confirmations est entre 4 et 8 caractere");
  	}
  	if (!strcmp($this->User['password'], $this->User['conf_password'])==0) {
  			$this->setError("le deux mot de pass doivent etre identique");
  			return $this->getError();
  	}


		return $this->getError();
	}

	public function reinitialize($data){
		$this->User=$data;
		// echo $this->getConfPassword();
 			if($this->getCode()==''){
				 $this->setError("le code ne peut pas etre vide ");
 		}
		if($this->getPassword()==''){
				 $this->setError("le mot de pass ne peut pas etre vide ");
		}
		if($this->getConfPassword()==''){
				 $this->setError("Veuillez confirmer le mot de pass");
		}
		if( strlen($this->getPassword()) < 8  ){
		  	 $this->setError("le mot de pass est entre 4 et 8 caractere");
  	}
  	if(strlen($this->getConfPassword() < 8 ) ) {
		  	 $this->setError("le mot de pass de confirmation est entre 4 et 8 caractere");
  	}
  	if (!strcmp($this->getPassword(), $this->getConfPassword())==0)   {
		  	 $this->setError("les deux mot de pass doivent etre identiques");
  	}
  	return $this->getError(); 
	}
	public function isValidInscriptionNewsletter(){
		if($this->User['email']==''){
				 $this->setError("l'email ne peut pas etre vide ");
		}

		if (!filter_var($data['email'],FILTER_VALIDATE_EMAIL)) {
			$this->setError("adres email incorect ") ;
		}
		
		return $this->getError();
	}

	public function viderLeCash(){
 		$this->User['email']='';
		$this->User['password']='';
	}


 }