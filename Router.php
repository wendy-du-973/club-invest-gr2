<?php
require_once('View/View.php');
require_once('Controller/Controller.php');
require_once('./Model/db.php');
require_once('./Model/UserRequest.php');
require_once('./Model/VoteRequest.php');
require_once('./Model/AuthenticationManager.php');


class Router
{
	private  $v;
	protected   $ctrl;
	protected   $AuthenticationManager;
	protected   $db;
	//  	function __construct(Controler $ctrl,View $v)
	// {	
	// 	$this->ctrl = $ctrl;			
	// 	$this->v =$v;
	// }
	public function getController()
	{
		return $this->ctrl;
	}
	public function getAuth()
	{
		return $this->AuthenticationManager;
	}


	public function  main()
	{
		$this->v =  new View($this);
		$this->UserRequest = new UserRequest();
		$v = $this->v;
		$UserRequest = $this->UserRequest;

		$this->AuthenticationManager = new AuthenticationManager($UserRequest);
		$AuthenticationManager = $this->AuthenticationManager;

		$this->ctrl = new Controller($v, $AuthenticationManager, $UserRequest);

		$c = $this->ctrl;
		if (!array_key_exists('page',$_GET)) {
			$v->showAcceuil();
		}
		if ($this->AuthenticationManager->isUserConnected()) { //}{
			
			// echo "<h1>";
			echo $this->AuthenticationManager->getUserName()['email'];
			// echo "</h1>";

			if ($this->AuthenticationManager->isComConnected()) {
				$v->showMenuCom();

				// $v->alertSuccess($v->setError("".$this->AuthenticationManager->getUserRole())); 
				// $v->showFooterOfLine();

			} else if ($this->AuthenticationManager->isComptaConnected()) {
				// code...
				$v->showMenuComPta();
			} else if ($this->AuthenticationManager->isAdminConnected()) {
				// $v->showMenuAdmin();
				// $v->desineAdminDashboard();
				// header("location:View/Pages/admin.php");
 				$c->giveAccesToDashboard();
			}
			$v->showAcceuil();

		} else {
			// $v->showHeader();
			$v->showMenuOffLine();
			// $v->showFooterOfLine();
			// "login" => " ?page=login",
			// $v->alertDanger($v->setError('vous etes not conected  <a href="index.php?page=login"> conectez-vous'));
			$v->showAcceuil();

		}

		if (!array_key_exists('page', $_GET)) {

			if (array_key_exists('edit', $_GET)) {
			$c->detailAction($this->getEditActionId());
			}


		} 



		if ($this->getUrlValue() === $this->getAcceuilUrl()) { /* acceuil */
			$v->showAcceuil();
		}
 
		if ($this->getUrlValue() === $this->getPresentationUrl()) { /* presentation */
			$v->showPresentation();
		}

		if ($this->getUrlValue() === $this->showContactUrl()) { /* contact */
			$v->showContactForm($_POST);
		}

		if ($this->getUrlValue() === $this->showLoginUrl()) { /* login */
			$v->showLogin($_POST);
		}

		if ($this->getUrlValue() === $this->getNewsletterUrl()) { /* newsletter */
			$v->showNewsLetterForm();
		}
		if ($this->getUrlValue() === $this->getPortefeuilUrl()) { /* portefeuil */
			$v->getPortefeuil();
		}
		if ($this->getUrlValue() === $this->getVoteUrl()) { /* Trading / achat */
			if ($this->getController()->getAuth()->isUserConnected()) {
				$v->getVote($_POST);
			}else{
				$this->getController()->getView()->alertSimpleUser();
				$this->getController()->getView()->showAcceuil();
			}
		}
		if ($this->getUrlValue() === $this->logout()) { /* logout*/
			$v->logout();
		}
		if ($this->getUrlValue() === $this->getFogotURl()) { /* logout*/
			$v->fogot($_POST);
		}
		if ($this->getUrlValue() === $this->sendNewsletterTrai()) { /* logout*/
			$c->sendNewsletterTrai($_POST);
		}
		if ($this->getUrlValue() === $this->sendInfoToEveryone()) { /* sendInfoToEveryone*/
			if ($this->getController()->isAdminConnected()) {
				$c->sendInfoToEveryone($_POST); 
			}else{
				$this->getController()->getView()->showAcceuil();
			}
		}

		if ($this->getUrlValue() === $this->getInscriptionURL()) { /* inscrire un nouveau membre **/
			if ($this->getController()->isAdminConnected()) {
				$v->getInscription($_POST);
			} else {
				$this->getController()->informAdminIntruisionByMail("d'inscrire une personne ");
				$this->getController()->getView()->alertIntruision();
			}
		}
		if ($this->getUrlValue() === 'admin') { /* admin a supprimer  */
			$v->showAdmin();
		}
		if ($this->getUrlValue() === $this->getForumUrl()) { /* admin a supprimer  */
			if ($this->getController()->getAuth()->isUserConnected()) {
    			$v->showForum();
			}else{
				$this->getController()->getView()->alertIntruision();
			}
		}


		if ($this->getUrlValue() === $this->sendNewsletterUrl()) { /* admin a supprimer  */
			$v->sendNewsletter($_POST);
		}

		if ($this->getUrlValue() === $this->sendMessageToForum()) { /* admin a supprimer  */
			$c->sendMessageToForum($_POST);
		}

		if ($this->getUrlValue() === $this->activeCompte()) { /* admin a supprimer  */
			$v->activeCompte($_POST);
		}
		if ($this->getUrlValue() === $this->sendInfoUrl()) { /* Enovoyer info a everbody  */
			// echo "sendInfoUrl";
			if ($this->getController()->getAuth()->isAdminConnected()) {
				$v->sendInfo($_POST);
			}else{
				$v->showAcceuil();
			}
		}
		if ($this->getUrlValue() === $this->fogotTrai()) { /* admin a supprimer  */
			$c->fogotTrai($_POST);
		}
		if ($this->getUrlValue() === $this->ouiVotes()) { /* effectuer un vote  */
			$c->ouiVotes($_POST);
		}
		if ($this->getUrlValue() === $this->endVotes()) { /* effectuer un vote  */
			if ($this->getController()->isUserLocked() !== 0) {
				$c->endVotes($_POST);
			} else {
				// $this->getController()->informAdminIntruisionByMail("lancer un vote"); /*** INFORM ADMIN ****/
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue() === $this->contactUsUrl()) { /* Contact us  */
			$c->contactUs($_POST);
		}
		if ($this->getUrlValue() === $this->startVote()) { /* Vote  */
			$c->startVote($_POST);
		}
		if ($this->getUrlValue() === $this->openSubjetUrl()) { /* forum  */
			$c->openSubject($_POST);
		}
		if ($this->getUrlValue() === $this->reinitialize()) { /* admin a supprimer  */
			$c->reinitialize($_POST);
		}
		if ($this->getUrlValue() === $this->getLockUser()) { /* bloquer / debloquer  */
			if ($this->getController()->isAdminConnected()) {
				$c->lockUser($_POST);
			} else {
				$this->getController()->getView()->alertIntruision();
			}
		}
		if ($this->getUrlValue()==$this->addActionUrl()) {       // show liste Action
			if ($this->AuthenticationManager->isUserConnected()) {
				$this->getController()->getView()->addAction($_POST);
			}else{
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue()==$this->getAddActionTrai()) {       // show addAction form 
			if ($this->AuthenticationManager->isUserConnected()) {
				$c->addAction($_POST);
			}else{
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue()===$this->editOrDeleteAction()) {
			if ($this->AuthenticationManager->isUserConnected()) {
						$c->editOrDeleteAction($_POST);
			}else{
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue() === $this->setRoleUrl()) { /* Changer le role dun membre  */
			if ($this->getController()->isAdminConnected()) {
				echo "setRole";
				$c->setRole($_POST);
			} else {
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue() === $this->a2f()) { /* admin a supprimer  */
			$c->a2f($_POST);
		}

		if ($this->getUrlValue() === $this->sendTchatUrl()) { /* admin a supprimer  */
			$c->sendTchat($_POST);
		}


		if ($this->getUrlValue() === $this->activeCompteTrai()) { /* activer compte   */
			$c->activeCompteTrai($_POST);
		}


		if ($this->getUrlValue() === $this->termineeSujetUrl()) { /* terminee sujet de forum   */
			$c->termineeSujet($_POST);
		}
		if ($this->getUrlValue() === $this->getListeMembre()) { /* activer compte   */
			if ($this->getController()->isAdminConnected()) {
				$c->getListeMembre();
			} else {
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue() === $this->getInscriptionNewsletter()) { /* activer compte   */
			$c->inscriptionNewsletter($_POST);
		}
		if ($this->getUrlValue() === $this->commentThisSujet()) { /* activer compte   */
			$c->commentThisSujet($_POST);
		}
		if ($this->getUrlValue() === $this->inscriptionTrai()) { /* inscrire un membre  */
			if ($this->getController()->isAdminConnected()) {
				$c->inscriptionTrai($_POST);
			} else {
				$this->getController()->getView()->alertIntruision();
			}
		}

		if ($this->getUrlValue() === $this->loginFormSubmit()) { /* admin a supprimer  */
			$c->loginFormSubmit($_POST);
		}
	}


	public function getUser()
	{
		return $this->AuthenticationManager->getUserName();
	}


	public function getPageUrlKeys()
	{
		return "page";
	}
	public function getUrlValue()
	{
		if (array_key_exists($this->getPageUrlKeys(), $_GET)) 
			if ($_GET)
				return $_GET[$this->getPageUrlKeys()];
			return null;
	}
	public function getApropos()
	{
		return "aPropos";
	}
	public function getAcceuilUrl()
	{
		return "acceuil";
	}

	public function getPresentationUrl()
	{
		return "presentation";
	}
	public function showContactUrl()
	{
		return "contact";
	}
	public function showLoginUrl()
	{
		return "login";
	}

	public function getForumUrl()
	{
		return "forum";
	}
	public function getTchatURL()
	{
		return "tchat";
	}
	public function getNewsletterUrl()
	{
		return "newsLetter";
	}
	public function getPortefeuilUrl()
	{
		return "portefeuil";
	}

	public function getVoteUrl()
	{
		return "vote";
	}
	public function loginFormSubmit()
	{
		return "loginFormSubmit";
	}
	public function a2f()
	{
		return 'a2f';
	}
	public function getInscriptionURL()
	{
		return "inscription";
	}

	public function inscriptionTrai()
	{
		return "inscriptionTrai";
	}
	public function logout()
	{
		return "logout";
	}

	public function activeCompte()
	{
		return "activeCompte";
	}

	public function activeCompteTrai()
	{
		return "activeCompteTrai";
	}
	public function getFogotURl()
	{
		return "fogot";
	}

	public function fogotTrai()
	{
		return "fogotTrai";
	}

	public function reinitialize()
	{
		return "reinitialize";
	}

	public function sendNewsletterUrl()
	{
		return "sendNewsletter";
	}
	public function sendNewsletterTrai()
	{
		return "sendNewsletterTrai";
	}
	public function getInscriptionNewsletter()
	{
		return "inscriptionNewsletter";
	}
	public function getListeMembre()
	{
		return "listeMembre";
	}

	public function getLockUser()
	{
		return "lock";
	}
	public function setRoleUrl()
	{
		return "setRole";
	}
	public function sendInfoUrl()
	{
		return "sendInfo";
	}
	public function sendInfoToEveryone()
	{
		return "sendInfoToEveryone";
	}

	public function contactUsUrl()
	{
		return "contactUsUrl";
	}
	public function startVote()
	{
		return "startVote";
	}

	public function ouiVotes()
	{
		return "ouiVotes";
	}
	public function getMenu()
	{
		$this->c->getMenu();
	}
	public function openSubjetUrl()
	{
		return "openSubject";
	}

	public function commentThisSujet()
	{
		return "commentThisSujet";
	}


	public function termineeSujetUrl()
	{
		return "termineeSujet";
	}


	public function endVotes()
	{
		return "endVotes";
	}


	public function sendMessageToForum()
	{
		return "sendMessageToForum";
	}

	public function sendTchatUrl()
	{
		return "sendTchat";
	}
	public function addActionUrl()
	{
		return "addAction";
	}

	public function getAddActionTrai(){
		return "addActionTrai";
	}	

	public function getEditActionId(){
		return $_GET['edit'];
	}
	public function editOrDeleteAction(){
		return "editOrDeleteAction";
	}
}
