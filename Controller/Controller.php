<?php 

require_once __DIR__ . '../../vendor/autoload.php';
require_once('./Model/db.php');
require_once('./Model/ActionRequest.php');
require_once('./Model/UserRequest.php');
require_once('./Model/ContactRequest.php');
require_once('./Model/RequestSubject.php');
require_once('./Model/CommentaireRequest.php');
require_once('./Model/VoteRequest.php');
require_once('./Model/inscriptionCodeRequest.php');
require_once('./Model/AuthenticationManager.php');
require_once('./Model/UserBuilder.php');
require_once('./Model/ActionBuilder.php');
require_once('./Model/CommentaireBuilder.php');
require_once('./Model/NewsBuilder.php');
require_once('./Model/VoteBuilder.php');
require_once('./Model/ContactBuilder.php');
require_once('./Model/SujetBuilder.php');
// ****************************************
require_once('./Model/db.php');


  		use OTPHP\TOTP;
		use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


/**
 * 
 */
class Controller
{
	
  private $ActionRequest;
  private $v;
  private $LieuRequest;
  private $AuthenticationManager;
 protected $mail ; 
 protected $RequestSubject ; 
 protected $inscriptionCodeRequest;
 protected $NewsRequest;

	public function __construct($v,$AuthenticationManager,$UserRequest)
	{
	$this->UserRequest = new UserRequest();
	$this->ActionRequest = new ActionRequest();
	
    $ActionRequest=$this->ActionRequest ;
    $this->v = $v;
    $this->AuthenticationManager = $AuthenticationManager;
    $this->inscriptionCodeRequest = new InscriptionCodeRequest();
    $this->RequestSubject = new RequestSubject();

    $this->mail= new PHPMailer(true);
	$this->mail->SMTPDebug = 0;                               
	$this->mail->IsSMTP();
	$this->mail->Host = 'smtp.gmail.com';               //Adresse IP ou DNS du serveur SMTP
	$this->mail->SMTPAuth = true;                        //Utiliser l'identification
	$this->mail->CharSet = 'UTF-8';

    $this->mail->SMTPSecure = "tls";     
    $this->mail->Port = 587;
   $this->mail->Username   =  'clubinvestest@gmail.com';    //Adresse email à utiliser
   $this->mail->Password   =  'Clubinvest14.';         //Mot de passe de l'adresse email à utiliser
	$this->mail->From = "clubinvestest@gmail.com"; //Adresse
	$this->mail->FromName = "Club d'Investissement";
	$this->mail->isHTML(true);

 	}


 	public function getView(){
 		return $this->v;	
 	}
 	public function getActionRequest(){
 		return $this->ActionRequest;	
 	}
 	public function getAuth(){
 		return $this->AuthenticationManager;
 	}
public function getUserRequest(){
	return $this->UserRequest;
}
public function getRequestSubject(){
	return $this->RequestSubject;
}
public function getAllSubject(){
	return $this->v->getAllSubject();
}

	public function loginFormSubmit($data){
		if ($_POST) {
			$userBuilder = new UserBuilder($data);
			$userBuilder->validLogin($data);
			if ($userBuilder->getError()==="") {
				if( $this->AuthenticationManager->connectUser($userBuilder->getEmail(),$userBuilder->getPassword())){
				$this->v->alertSuccess($this->v->setError(" Vous y etes presque. plus q'un pas ... ".$this->AuthenticationManager->getUserName()['prenom']));
					header("location:index.php");

				// *****************************
				   // A2F
				// $this->v->showA2F($this->AuthenticationManager->getUserName());
			// **********************

			}else{
				$this->v->alertDanger($this->v->setError("No user found "));
				$this->v->showLogin($data);
			}
			}else{
				$this->v->alertDanger($this->v->setError($userBuilder->getError()));
			}
			// ************
			
		}else{
				$this->v->alertDanger($this->v->setError("formulaire nin soumis "));
		}
		// var_dump($data);
		$this->v->showLogin($data);
	}
	public function detailAction($id){
		$ActionRequest = new ActionRequest();
		$action= $ActionRequest->findActionById($id);
		$this->v->showDetailAction($action);

	}
	public function editOrDeleteAction($data){
		$ActionRequest = new ActionRequest();

		if (isset($_POST['edit']) ) {
			$actionBuilder = new ActionBuilder($data);
			$actionBuilder->isValideCreate($data);
			if ($actionBuilder->getError()=="") {
				if ($ActionRequest->setAction($data)>0) {
					$this->v->alertSuccess($this->v->setError("modification effectuee avec succees"));
					$this->v->showDetailAction($data);
				}else{
					$this->v->alertDanger($this->v->setError("Modification non effectuee"));
					$this->v->showDetailAction($data);
				}
			}else{
				$this->v->alertDanger($this->v->setError($actionBuilder->getError()));
					$this->v->showDetailAction($data);

			}
		}
		else if (isset($_POST['delete']) ) {
			// 			$actionBuilder = new ActionBuilder($data);
			$actionBuilder = new ActionBuilder($data);
			$actionBuilder->isValideCreate($data);
			
			if ($actionBuilder->getError()=="") {
				if ($ActionRequest->deleteAction($data)>0) {
					$this->v->alertSuccess($this->v->setError("Action supprimee avec succees"));
					$this->v->addAction($data);
				}else{
					$this->v->alertDanger($this->v->setError("suppression non effectuee"));
					$this->v->showDetailAction($data);
				}
			}else{
				$this->v->alertDanger($this->v->setError($actionBuilder->getError()));
					$this->v->showDetailAction($data);

			}



		}else{
				$this->v->alertDanger($this->v->setError("formulaire nin soumis "));
		}


	}
	public function addAction($data){

		$actionBuilder = new ActionBuilder($data);
		$actionBuilder->isValideCreate($data);
		if ($actionBuilder->getError()=="") {
			$ActionRequest = new ActionRequest();
			if($ActionRequest->isDoublon($data)>0 ){
				$this->v->alertSuccess($this->v->setError("Nouvelle Action ajoutee avec succes !"));
				$this->v->addAction($data);

			}else{
				$this->v->alertDanger($this->v->setError("cette action existe deja !"));
				$this->v->addAction($data);

			}
			// code...
		}else{
				$this->v->alertDanger($this->v->setError($actionBuilder->getError()));
				$this->v->addAction($data);
		}

	}
	public function a2f($data){

		$otp=TOTP::create('FNC7GMRXKHTHVLKHTRFRIAQ2D4YGN5PTNG3T3UWXYX4PFOOOZMAFUEOLW4JAHQK37T736JKFV3QG5UYC3Y7XVJ4BTUGAEZK7C4LLP7Q');
		if($_POST ){
			if($otp->verify(htmlspecialchars(trim($_POST['code'])))){
				$this->v->alertSuccess($this->v->setError(" Bienvenue ".$this->AuthenticationManager->getUserName()['prenom']));
			header("location:index.php");
			exit();
 			}else{
				$this->v->alertDanger($this->v->setError("Code Incorrect Reessayer ! "));
				$this->v->showA2F($data);
			}
		}
 	}
 
 	public function inscriptionTrai($data){
 		if($_POST['inscription']){
 			// var_dump($data);
  			$userBuilder = new UserBuilder($data);

  			$userBuilder->valideInscriptionUser($data);
 			if($userBuilder->getError()!=="") {
 								$this->v->alertDanger($this->v->setError($userBuilder->getError()));
	 			$this->v->getInscription($data);
 			}
 			else{ 
 						if ($this->UserRequest->findUserByEmail(htmlspecialchars($data['email']))>0) {
								$this->v->alertDanger($this->v->setError("cet utiliateur exite deja "));
 						}else{
 							// var_dump($this->UserRequest->newUser($data)); 
								if($this->UserRequest->newUser($data) ){
				 				  if($this->sendCodeMailToUserInscription($data['email'])){
							 				$this->v->alertSuccess($this->v->setError("creation de compte reussi !"));
							 		$this->getListeMembre();
									}else{
										$this->v->alertDanger($this->v->setError("echec de creation du compte "));
									}
							}else{
							   	$this->v->alertDanger($this->v->setError("une Erreur s'est produite"));
									}
 						}
			 		
	 		}
 		}else{
				$this->v->alertDanger($this->v->setError("formulaire non soumis"));
			}
 	}
 	public function getRandomSixDigit(){
 		return rand(100000,999999);
 	}
 	public function  sendCodeMailToUserInscription($userEmail){
 		// $userEmail="clubinvestest@gmail.com";

 		$data=$this->getRandomSixDigit();
		$this->mail->addAddress($userEmail, "");
		// $this->inscritptionCode($userEmail,$data);
 		$this->UserRequest->inscritptionCode($userEmail,$data); 

 		$url ='';
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
	    $url = "https";
	}else{
	    $url = "http"; 
	}  
$url .= "://"; 
$url .= $_SERVER['HTTP_HOST']; 
$url .= $_SERVER['REQUEST_URI']; 

	$this->mail->Subject = "votre code d'activation de compte ";
	$this->mail->Body = "<h4>Bonjour, <br> veuillez trouver ci-joint le code qui vous permettera d'activer
	votre compte . <br> <i> Attention </i> il n'est valide que 24h </p> passer ce delai il vous faudra demaner a votre admin de vous fournir un autre code </p></h4>
	ou de vous donner acces a votre compte 
	<h2> Code : $data   </h2>
	";
	// $this->mail->AltBody = "This is the plain text version of the email content";
	return $this->mail->send();	
 		
 	}

 	public function inscritptionCode($email,$code){
 		$this->UserRequest->inscritptionCode($email,$code);
 	}
 	public function informAdminIntruisionByMail($problemDescription){
		$userEmail="clubinvestest@gmail.com";
		$this->mail->addAddress($userEmail, "");
		$this->mail->Subject = "Alert Intruision ";
		$coupable= $this->getAuth()->getUserName()['prenom'];
		$this->mail->Body = "<h4>Attention,  $coupable a essayer $problemDescription de maniere louche <br></h4>	";
	return $this->mail->send();	

 	}
 	public function sendMailConfirmActivationCompte($userEmail){

 	}
 	 
 	public function sendMailToConfirmContactReception($email,$contenu){
 		$userEmail='clubinvestest@gmail.com';
		$this->mail->addAddress($userEmail, "");

		$this->mail->Subject = " Nous Avons Ete Contacté ";
		$this->mail->Body = "<h4> $contenu
		 </h4>
		 <h2> de la part de $email </h2>";
		// $this->mail->AltBody = "This is the plain text version of the email content";
		return $this->mail->send();	
 	}
 	public function sendMailToInformVote(){
 		return true;
 	}
 	public function sendMailConfirmInscriptionNewsletter($userEmail){
 		// $userEmail="clubinvestest@gmail.com";
		$this->mail->addAddress($userEmail, "");

		$this->mail->Subject = " Confirmation d'inscription a notre Newsletter";
		$this->mail->Body = "<h4>Bonjour, <br>Ceci est un mail de confirmation de votre inscription a notre Newsletter.
		 </h4>
		 <h2> Nous vous souhaitons la bienvenue parmi nous </h2>";
		// $this->mail->AltBody = "This is the plain text version of the email content";
		return $this->mail->send();	
 	}

 	public function notifyInfoLockUser($user){
 		$userEmail=htmlspecialchars(trim($user['email']));  
 		$this->mail->addAddress($userEmail, "");
 		if (strcmp($user['status'],'lock')==0) {
		$this->mail->Subject = " Restrictoin d'acces";
		$this->mail->Body = "<h2>Bonjour, <br> 
			vous avez perdu vos droits d'acces a nos services.
		 </h2>
		 <h4> contactez votre admin </h4>"; 		
	}else {
			$this->mail->Subject = " De Nouveau Acces A Votre Compte ";
		$this->mail->Body = "<h2>Bonjour, <br> 
			vous  pouvez a present utiliser votre compte 
		 </h2>
		 <h4> Ravis de vous avoir parmis nous </h4>"; 		
	}
		return $this->mail->send();	

		// $this->mail->AltBody = "This is the plain text version of the email content";
 	}
 	public function activeCompteTrai($data){
 		// var_dump($data);
 		if ($_POST) {
 			$UserRequest = new UserRequest();
			$userBuilder = new UserBuilder($data);
			$userBuilder->actviationCompteValidation($data);
 		if($userBuilder->getError()==''){
  			$code = $this->inscriptionCodeRequest->findCode($data['email'] )  ;
 
	 		if($code['code'] === $userBuilder->getCode()){
	 			$user= $UserRequest->findUserByEmail($userBuilder->getEmail()) ;
				if($UserRequest->activateAccount( $user)>0){

				var_dump($user);
						$UserRequest->deleteDataResetPassword($data);
						$this->v->alertSuccess($this->v->setError('Activation reussi ! Vous pouvez a present 	vous authentifiez'));
						$this->activateCompteByEmail($userBuilder->getEmail());
							$this->v->showLogin($data);
 


				$this->v->showLogin($data);
				// header("Location:index.php");
				}else{
				    $this->v->alertDanger($this->v->setError("votre compte n'a pas ete active "));
			
			}
			}else{
			    $this->v->alertDanger($this->v->setError('mauvais code'));
				$this->v->activeCompte($data);
			}


 		}
 		else{
		    $this->v->alertDanger($this->v->setError($userBuilder->getError()));
			$this->v->activeCompte($data); 
 		}
 	}else{
	    $this->v->alertDanger($this->v->setError('formulaire non soumis'));
	}
 		 	// 	var_dump($this->inscriptionCodeRequest->findCode($data['email']) );
 		$this->v->activeCompte($data);
 	}
 	public function activateCompteByEmail($email){
		// $userEmail='clubinvestest@gmail.com';
		$this->mail->addAddress(htmlspecialchars(trim($email)), "");
				$this->mail->Subject = " Activation de Votre Compte Reussi !";
		$this->mail->Body = "<h2>Bonjour, <br> 
			Votre Compte a ete active avec succes. 
			Vous pouvez des a present vous rendre sur votre espace
		 </h2>

		 <h4> Nous sommes ravis de vous comptez parmis nous </h4>"; 		
		return $this->mail->send();	

 	}

public function endVotes($data){
	if ($_POST['Endvote']) {
 		if ($this->isUserLocked()!==0) {
			if ($this->isVotingNow()) {
						if ($this->isItYourVote()) {
								$VoteBuilder = new VoteBuilder($data);
								if ($VoteBuilder->isValidVote()) {
									$VoteRequest = new VoteRequest();
									if($VoteRequest->endVotes()){
								    $this->v->alertSuccess($this->v->setError("Le Vote est terminee"));						
									}	else{
								    $this->v->alertDanger($this->v->setError("Une Erreur s'est produite "));						
									}
								}
							}else{
							    $this->v->alertDanger($this->v->setError('vous ne pouvez pas mettre fin a ce vote'));
							  }

			}else{
				    $this->v->alertDanger($this->v->setError("il n'y a pas de vote"));
			}
		}else{
			 $this->getView()->userIsLocked();
		}
	}
}

	public function sendCodeToResetPassword($email){
		$code=$this->getRandomSixDigit();
  		$UserRequest = new UserRequest();

		$this->mail->addAddress($email, "");
		// $this->inscritptionCode($userEmail,$data);
 		$this->UserRequest->sendCodeToResetPassword($email,$code); 

	$this->mail->Subject = "Reinitialiser Votre Mot De Pass ";
	$this->mail->Body = "<h4>Bonjour, <br> veuillez trouver ci-joint le code qui vous permettera de reinitializer Votre mot de pass . <br> <i> Attention </i> il n'est valide que 24h </p> passer ce delai il faudra recommencer  </p></h4>
		ou de vous donner acces a votre compte 
		<h2> Code : $code   </h2>
	";
	// $this->mail->AltBody = "This is the plain text version of the email content";
	return $this->mail->send();	

	}

 	public function fogotTrai($data){

 		  $userBuilder = new UserBuilder($data);
 		  $userBuilder->isValidResetPassword($data);
 		  if ($userBuilder->getError()=="") {
		  		if($this->sendCodeToResetPassword(htmlspecialchars(trim($data['email'])))  ){
			  		$this->v->reinitialize($data);

		  		}else{
				    $this->v->alertDanger($this->v->setError("une ereur s'est produite "));
		  		}
 		  }else{
				    $this->v->alertDanger($this->v->setError($userBuilder->getError()));
				    $this->v->fogot($data);
 		  }

 	}
 	public function reinitialize($data){
 	
 		if ($_POST) {
 					$userBuilder = new UserBuilder($data);
			 		if ($userBuilder->reinitialize($data)=='') {
			 			$UserRequest = new UserRequest();
			 			if($UserRequest->findEmailAndCodeToResetPassword($data) ){
			 				if($UserRequest->resetPassword($data)){
					    	$this->v->alertSuccess($this->v->setError(" password changed successfully"));
					    	   $UserRequest->deleteDataResetPassword($data);
					    	   $this->v->showLogin($data);

							}else{
					    	$this->v->alertDanger($this->v->setError("UneErreur s'est produite Veuillez recommencer"));
					    }
			 			}else{
					    	$this->v->alertDanger($this->v->setError("Une Erreur s'est produite Veuillez recommencer"));
						}
						$this->v->reinitialize($data); 

			 		}else{

					    $this->v->alertDanger($this->v->setError($userBuilder->getError()));
						$this->v->reinitialize($data); 
			 		}
		 		}else{
					    	$this->v->alertDanger($this->v->setError("formulaire non soumis !"));
					    }
 	}

 	public function sendNewsletterEmail($emailList,$infoTitre,$infoContenu){
 		for ($i=0; $i < count($emailList); $i++) { 
		 		$UserRequest = new UserRequest();
		 			$this->mail->addAddress($emailList[$i]);

				$this->mail->Subject = $infoTitre;
				$this->mail->Body =$infoContenu;
				// $this->mail->AltBody = "This is the plain text version of the email content";
			}

		return $this->mail->send();	
 	}
 	public function sendNewsletterTrai($data){
 		if($_POST){
 			$newsBuilder = new NewsBuilder($data);
 			$UserRequest = new UserRequest();
 			$newsBuilder->sendInfoValid($data); 
 			if($newsBuilder->getError()=="") { 

 				$newsRequest = new NewsRequest();
 				/**** Envoyer l'email ******/
 			// $listeMail[] = ($UserRequest->getAllEmail() );
 				// var_dump($UserRequest->getAllEmail() ) ; 
 				$newsRequest->sendNewsletter($data);

 				$listeMail =array();

 				foreach($UserRequest->getExternalUserEmail() as $emailArr =>$emailValue){
 					foreach ($emailValue as $emailId ) {
 						if (filter_var($emailId,FILTER_VALIDATE_EMAIL)) {
			 				$listeMail [] = $emailId;
 						}
 					}
 				}
 				$newsRequest->sendNewsletter($data);
 				for ($i=0; $i < count($listeMail); $i++) { 
 	 					if($this->sendNewsletterEmail($listeMail,$data['titre'],$data['contenu']) ){
						    $this->v->alertSuccess($this->v->setError("Newsletter envoyees avec succes "));
						    $this->v->sendNewsletter($data);	
						    
						}else{
						    $this->v->alertDanger($this->v->setError("une erreur s'est produite "));
						    $this->v->sendNewsletter($data);	
						}
 				}
 
  			}else{
			    $this->v->alertDanger($this->v->setError($newsBuilder->getError()));
			    $this->v->sendNewsletter($data);
 			}
 		}else{
			    $this->v->alertDanger($this->v->setError(" formulaire non soumis "));
			}
 		// $this->getView()->sendNewsletter($data);
  	}
 	public function redirectionToURL($url){
 		header("Location:index.php?page=$url");
 	}
 	public function getAllNewsletter(){
 		$NewsRequest = new NewsRequest();
 		return $NewsRequest->getAllNewsletter(); 
 	}
 	public function inscriptionNewsletter($data){
 		if($_POST){ 
 				$userBuilder = new UserBuilder($data);
 				if($userBuilder->isValidInscriptionNewsletter($data)){

 					$UserRequest = new UserRequest($data);
 					if($UserRequest->findUserByEmail($data['email'])>0){
					    $this->v->alertDanger($this->v->setError(" vous etes deja inscrits chez nous "));

 					}else{
 						if($UserRequest->inscriptionUser($data['email'])){
					   $userBuilder->viderLeCash();
					   if($this->sendMailConfirmInscriptionNewsletter($data['email'])){
					   	  $this->v->alertSuccess($this->v->setError("inscription effectuee avec succees"));
					   		$data='';

					   }else{
						    $this->v->alertDanger($this->v->setError("une erreur s'est produite "));

					   }
		 				}else{
						    $this->v->alertDanger($this->v->setError("une erreur s'est produite "));

		 				}	
 					}
	 				
 				}
 				$this->v->showAcceuil();
 			
 		}
 	}





 	public function getListeMembre(){ 		
 		$this->v->listeMembre();
 	}
 	public function lockUser($data){
 		if($_POST){
 			if($this->AuthenticationManager->isAdminConnected()){

	 				$UserRequest = new UserRequest();
	 				$user =$UserRequest->findUserById($data['id']);
	 				if($UserRequest->lockOrUnlock($user)){
					    $this->v->alertSuccess($this->v->setError("Modificatin Effetuee avec succes !"));
					    $this->notifyInfoLockUser($user);
 					    header("Location:index.php");
 	 				}else{
					    $this->v->alertDanger($this->v->setError("Modificatin Non Effetuee !"));
	 				}
	  				$this->v->listeMembre($UserRequest->findAllUsers());
  			}else{
				    $this->v->alertDanger($this->v->setError("Alert Intruision !"));
				    // $this->notifIntruision("quelquun essaye de bloquer un utilisateur");
				}

 		}else{
				    $this->v->alertDanger($this->v->setError("ce formulaire n'a pas ete soumis !"));
		}
				$this->getView()->listeMembre();

 	}
 	public function redirectionToindex(){
 		header('Location:index.php');
 	}
 public function setRole($data){
 	if($_POST){
 		var_dump($data);
 		if($this->AuthenticationManager->isAdminConnected()){
			$UserRequest = new UserRequest();
 			$user =$UserRequest->findUserById($data['role']);
 			if($user['role'] !=="admin"){
 				if($UserRequest->setRole($data)){
 					header("location:index.php");
				    $this->v->alertSuccess($this->v->setError(" desormais ".$user['prenom']." est".$data['role']));

 				}else{
				    $this->v->alertDanger($this->v->setError("une Erreur s'est produite !"));
 				}
 			}
 		}else{
				    $this->v->alertDanger($this->v->setError("Alert Intruision !"));
				    // $this->notifIntruision("quelquun essaye de bloquer un utilisateur");
		}
 	}
 }
 public function isAdminConnected(){
 	return $this->AuthenticationManager->isAdminConnected();
 }
 public function sendInfoToEveryone($data){
 	if($_POST){
 		$NewsBuilder = new NewsBuilder($data);
 		$NewsBuilder->sendInfoValid($data);
 			if ($NewsBuilder->getError()=="") {
	 		$NewsRequest = new NewsRequest();
	 		if($NewsRequest->sendRequestToEveryone($data)){
	 			$UserRequest = new UserRequest();
 				$listeMail =array();

 				foreach($UserRequest->getAllEmail() as $emailArr =>$emailValue){
 					foreach ($emailValue as $emailId ) {
 							if (filter_var($emailId,FILTER_VALIDATE_EMAIL)) {
				 				$listeMail [] = $emailId;
 							}
 					}
 				}

	 			// var_dump($listeMail);
	 			if($this->sendInfoEmail($listeMail,$NewsBuilder->getTitle(),$NewsBuilder->getContenu())){
				    $this->v->alertSuccess($this->v->setError( "information envoyer avec succes "));
				    $this->v->sendInfo($data);
	 			}else{
				    $this->v->alertDanger($this->v->setError( "une erreur s'est produite  "));
				    $this->v->sendInfo($data);

	 			}
	 		}else{
				    $this->v->alertDanger($this->v->setError( "requette non execu "));
				}	

 		}else{
		    $this->v->alertDanger($this->v->setError($NewsBuilder->getError()));
 			$this->v->sendInfo($data);
 		}
 	}
 	// $this->v->showAcceuil();
 }
 public function sendInfoEmail($listeMail,$title,$contenu){
 		for ($i=0; $i < count($listeMail); $i++) { 
		 			$this->mail->addAddress($listeMail[$i]);

				$this->mail->Subject = $title;
				$this->mail->Body =$contenu;
				// $this->mail->AltBody = "This is the plain text version of the email content";
			}
			return $this->mail->send();
 }
public function contactUs($data){
	if($_POST){
 		$contactBuilder = new ContactBuilder($data);
		if(empty($data['email']) ||  empty($data['contenu'])    )   {
		    	$this->v->alertDanger($this->v->setError( " Veuillez remplir les champs avant "));
		    	$this->getView()->showContactForm($data);
		}else{
			if (!filter_var($data['email'],FILTER_VALIDATE_EMAIL) ) {
			    	$this->v->alertDanger($this->v->setError( "verifier l'ortographe de vote adress email "));
		    	$this->getView()->showContactForm($data);

			}else{


  		$ContactRequest = new ContactRequest($data);
			if($ContactRequest->newContact($data)){
				// if($this->sendMailToConfirmContactReception($data['email'],$data['contenu']) );
				// htmlspecialchars($data['contenu']))){
				    $this->v->alertSuccess($this->v->setError( " Nos conseillers vous repondrons sous peu"));				
				    $this->v->showAcceuil();	
				// }else{
			    	// $this->v->alertDanger($this->v->setError( "une erreur s'est produite  "));
			    	// $this->v->showContactForm($data);
				// }
			}
		}
		}
	}
}


 
public function liseVotes(){
	$VoteRequest = new VoteRequest();
	$this->v->liseVotes($VoteRequest->findAllVotes());	
}
public function isVotingNow(){
	$VoteRequest = new VoteRequest();
	return $VoteRequest->isVoteDoing()>0;
 }

public function startVote($data){
	if($_POST){
  		$VoteBuilder = new VoteBuilder($data);
  		if(!empty(htmlspecialchars(trim($data['sujet']))) and !empty(htmlspecialchars(trim($data['duree'])))) {
			$VoteRequest = new VoteRequest();
				if($VoteRequest->isVoteDoing()>0){
			    	$this->v->alertDanger($this->v->setError( "un vote est deja encours  "));
				}else {
					if($this->isUserLocked()!==0){
 							if($VoteRequest->newVote($data) and $this->sendMailToInformVote()){
 								    $this->v->alertSuccess($this->v->setError( "le vote a debuter "));			
								    $this->v->getVote($data);	
								}else{
							    	$this->v->alertDanger($this->v->setError( "une erreur s'est produite  "));
								    $this->v->getVote($data);	
								}
							// }	
 						}else{
				    	$this->v->alertDanger($this->v->setError( " Vous n'avez pas le droit au vote. Contactez votre admin  "));
						}
			    $this->v->getVote($data);	

				
		}

		}else{
	    	$this->v->alertDanger($this->v->setError( "veuillez remplir les champs ".$VoteBuilder->getError()));
		}
	}
	// $this->getView()->getVote();
	// $this->liseVotes();
}
public function ouiVotes($data){
	if($_POST){
    		$VoteBuilder = new VoteBuilder($data);
		$VoteRequest = new VoteRequest();
		if( $this->AuthenticationManager->getUserName()['id']===trim($data['idUser'])){
 			if (isset($_POST['choix'])) {
 					$this->voteNow($data);
			}else {
				$this->v->alertDanger($this->v->setError("Veuillez faire votre choix du vote avant "));
			}
		}
 
	}
}

public function voteNow($data){

	$VoteRequest = new VoteRequest();

	// echo $VoteRequest->isVoteDoing()['oui'];
if ($VoteRequest->hasAllreadyVoted(htmlspecialchars(trim($data['idUser'])))>0) {
				$this->v->alertDanger($this->v->setError(" votre vote a deja ete pris en compte "));

}else{
		if($VoteRequest->upgradeVote($data) ){
			if($VoteRequest->inscritVotant(htmlspecialchars(trim($data['idUser']))) ){
				$this->v->alertSuccess($this->v->setError("votre vote a ete pris en compte "));
				$this->liseVotes();

			}else{
				$this->v->alertDanger($this->v->setError("une eureur s'est produite "));
				$this->liseVotes();

			}
 	}else{
			$this->v->alertDanger($this->v->setError("une eureur s'est produite "));

	}

}
$this->liseVotes();
}
public function hasAllreadyVoted(){
	$VoteRequest = new VoteRequest();
	 
	return $VoteRequest->hasAllreadyVoted($this->AuthenticationManager->getUserName()['id']);
}
public function isUserLocked(){
	return strcmp($this->AuthenticationManager->getUserName()['status'], 'lock');
}
public function getCurrentVote(){
		$VoteRequest = new VoteRequest();
		
		return $VoteRequest->isVoteDoing();

}
public function isItYourVote(){
	$VoteRequest = new VoteRequest();
	return ($this->AuthenticationManager->getUserName()['id']==$VoteRequest->isvoteDoing()['idUser']);
}
public function nbVotant(){
	$VoteRequest = new VoteRequest();
	return $VoteRequest->nbVotant();
 }
public function nbUserCanVote(){
	$VoteRequest = new VoteRequest();
	return $VoteRequest->ubuserCanVote();
 }

public function getAllSujet(){
	$RequestSubject = new RequestSubject();
	return $RequestSubject->findAllSujet();
	// // $tdeis->v->getAllSujet($RequestSubject->findAllSujet());	
 // 	foreach ($RequestSubject->findAllSujet() as $sujet ) {
	// 	 var_dump($sujet);

	// 	 $this->v->getSujet($sujet);
	// 	$commentaireRequest = new CommentaireRequest();
	// 	foreach($commentaireRequest->getCommentsForSujet($sujet['id']) as $commentaire ){
	// 		$this->v->getCommentsForSujet($commentaire);
	// 	}
	// 	// $this->getAllCommentForThisSujet($sujet['id']);
	// 	$this->commentThisSujet($sujet);
	// }
}
public function getAllCommentForThisSujet($sujetId){
	$commentaireRequest = new CommentaireRequest();
	 // var_dump($commentaireRequest->getCommentsForSujet($sujetId) );
	 $this->v->getCommentsForSujet( $commentaireRequest->getCommentsForSujet($sujetId) );
	// $this->v->getAllSujet($RequestSubject->findAllSujet());	

}
public function sendMessageToForum(){
	echo "sendMessageToForum";
	?>
	<?php  
}
public function commentThisSujet($commentaire){
	if($this->AuthenticationManager->getUserName()['id']==$commentaire['idUser']){

		$commentaireBuilder = new CommentaireBuilder($commentaire);
		if($commentaireBuilder->isValid()){
			$commentaireRequest = new CommentaireRequest();
			if ($commentaireRequest->commentAsubject($commentaire)) {
				$this->v->alertSuccess($this->v->setError("votre avis a bien ete pris en compte"));
			}else{
				$this->v->alertDanger($this->v->setError("votre avis n'a pas ete pris en compte"));

			}
			$this->v->showForum();
		}else{
			$this->v->alertDanger($this->v->setError($commentaireBuilder->getError()));
		} 
	}else{		
		$this->v->alertDanger($this->v->setError("veuillez utiliser votre compte pour cela"));
	}
	$commentaireBuilder->destroyData();
}
  public function openSubject($data){

 	 if ($_POST) {
  	 	$sujetBuilder = new SujetBuilder($data);
  	 	if ($sujetBuilder->isValid()) {
  	 		// code...
  	 		$RequestSubject = new RequestSubject();
  	 		// var_dump($RequestSubject->findAllSujet());
  	 		// $this->v->startListeSubject();
  	 		if ($RequestSubject->newSujet($data)) {
    			$this->v->alertSuccess($this->v->setError("nouveau Sujet"));
  	 		}else{
			$this->v->alertSuccess($this->v->setError("une erreur s'est produite"));
  	 		}
  	 	}
		// if($data['msg']===" " ){
			// $this->v->alertDanger($this->v->setError("Veuillez remplir les chmamps svp"));
 	 	// }else{
			// $this->v->alertSuccess($this->v->setError($data['msg']));

 	 	// }
 	 
	 }
	 	$this->v->showForum();


}
public function termineeSujet($data){
	 if($_POST){
		var_dump($data);
		if($this->AuthenticationManager->getUserName()['id']==$data['idUser']){

		$sujetBuilder = new SujetBuilder($data);
		if($sujetBuilder->isValid()){
			$RequestSubject = new RequestSubject();
			if($RequestSubject->endSujet($data)){ // on doit gerer le cas des doublons aussi
				$this->v->alertSuccess($this->v->setError("Sujet Clos"));
			}else{
				$this->v->alertDanger($this->v->setError("Une Erreur s'est produite "));
				$this->v->showForum();
			}	
		}
		$this->v->showForum();
	}
	$this->v->showForum();
	}else{
		$this->v->alertDanger($this->v->setError("secr"));
	}
	$this->v->showForum();
	$sujetBuilder->destroyData();
}

public function giveAccesToDashboard(){
	$this->getView()->dashboardPage();
}


} 